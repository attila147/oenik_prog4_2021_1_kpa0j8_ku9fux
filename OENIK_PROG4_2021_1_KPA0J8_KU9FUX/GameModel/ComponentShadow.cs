﻿// <copyright file="ComponentShadow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Component shadow class.
    /// </summary>
    public class ComponentShadow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentShadow"/> class.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="owner">name.</param>
        /// <param name="connectedIds">connecteds.</param>
        /// <param name="chargeLevel">charge level.</param>
        /// <param name="isAttacked">is attacked.</param>
        /// <param name="connectedTOus">connected to us.</param>
        /// <param name="connectedTOhim">connected to him.</param>
        public ComponentShadow(int id, string owner, int[] connectedIds, int chargeLevel, bool isAttacked, bool connectedTOus, bool connectedTOhim)
        {
            this.Id = id;
            this.Owner = owner;
            this.ConnectedIds = connectedIds;
            this.ChargeLevel = chargeLevel;
            this.IsAttacked = isAttacked;
            this.ConnectedTOus = connectedTOus;
            this.ConnectedTOhim = connectedTOhim;
        }

        /// <summary>
        /// Gets or sets component id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets component owner.
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// Gets or sets component connected ids.
        /// </summary>
        public int[] ConnectedIds { get; set; }

        /// <summary>
        /// Gets or sets component charge level.
        /// </summary>
        public int ChargeLevel { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether component is attacked.
        /// </summary>
        public bool IsAttacked { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether component connected to him.
        /// </summary>
        public bool ConnectedTOhim { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether component connected to us.
        /// </summary>
        public bool ConnectedTOus { get; set; }
    }
}
