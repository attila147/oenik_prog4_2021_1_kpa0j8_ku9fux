﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<the array is fine here.>", Scope = "member", Target = "~P:CircuitWars.GameModel.IGameModel.Components")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<the array is fine here.>", Scope = "member", Target = "~P:CircuitWars.GameModel.IGameModel.Walls")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<security here is not important.>", Scope = "member", Target = "~P:CircuitWars.GameModel.IGameModel.Threads")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Need of that setter.>", Scope = "member", Target = "~P:CircuitWars.GameModel.IGameModel.Threads")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<the array is fine here.>", Scope = "member", Target = "~P:CircuitWars.GameModel.Component.ConnectedIds")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<the array is fine here.>", Scope = "member", Target = "~P:CircuitWars.GameModel.Component.PossibleThreadsIDS")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<the array is fine here.>", Scope = "member", Target = "~P:CircuitWars.GameModel.GGameModel.Components")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<the array is fine here.>", Scope = "member", Target = "~P:CircuitWars.GameModel.GGameModel.Walls")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Uninteresting>", Scope = "member", Target = "~P:CircuitWars.GameModel.GGameModel.Threads")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Uninteresting>", Scope = "member", Target = "~P:CircuitWars.GameModel.Thread.Electrons")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Uninteresting>", Scope = "member", Target = "~P:CircuitWars.GameModel.ComponentShadow.ConnectedIds")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "Uninteresting")]
