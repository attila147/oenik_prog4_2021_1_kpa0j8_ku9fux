﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Interface for the game model, includes all data structures.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets stores components (active elements of the game).
        /// </summary>
        public Component[] Components { get; set; }

        /// <summary>
        /// Gets or sets stores walls (Passive elements).
        /// </summary>
        public Wall[] Walls { get; set; }

        /// <summary>
        /// Gets the width property of the game, in tile.
        /// </summary>
        public double GameWidth { get; }

        /// <summary>
        /// Gets the height property of the game, in tile.
        /// </summary>
        public double GameHeight { get; }

        /// <summary>
        /// Gets or sets the tilesize property of the game, in tile.
        /// </summary>
        public double TileSize { get; set; }

        /// <summary>
        /// Gets or sets the threads list.
        /// </summary>
        public List<Thread> Threads { get; set; }

        /// <summary>
        /// Gets or sets players name for save.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets level played on.
        /// </summary>
        public string ActLevel { get; set; }

        /// <summary>
        /// Gets or sets mode played on.
        /// </summary>
        public int Mode { get; set; }

        /// <summary>
        /// Gets or sets time gone.
        /// </summary>
        public TimeSpan Time { get; set; }
    }
}