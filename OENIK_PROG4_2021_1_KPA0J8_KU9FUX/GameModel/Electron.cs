﻿// <copyright file="Electron.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Electron class.
    /// </summary>
    public class Electron
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Electron"/> class.
        /// </summary>
        /// <param name="owner">owner.</param>
        /// <param name="point">point.</param>
        /// <param name="charge">charge.</param>
        /// <param name="actX">actx.</param>
        /// <param name="actY">acty.</param>
        /// <param name="stepX">stepx.</param>
        /// <param name="stepY">stepy.</param>
        public Electron(string owner, Point point, int charge, decimal actX, decimal actY, decimal stepX, decimal stepY)
        {
            this.Owner = owner;
            this.Point = point;
            this.Charge = charge;
            this.ActX = actX;
            this.ActY = actY;
            this.StepX = stepX;
            this.StepY = stepY;
            this.Rect = new Rect(new Point(point.X, point.Y), new Size(10, 10));
        }

        /// <summary>
        /// Gets or sets electron owner.
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// Gets or sets electron point.
        /// </summary>
        public Point Point { get; set; }

        /// <summary>
        /// Gets or sets electron charge.
        /// </summary>
        public int Charge { get; set; }

        /// <summary>
        /// Gets or sets electron rect.
        /// </summary>
        public Rect Rect { get; set; }

        /// <summary>
        /// Gets or sets electron actx.
        /// </summary>
        public decimal ActX { get; set; }

        /// <summary>
        /// Gets or sets electron acty.
        /// </summary>
        public decimal ActY { get; set; }

        /// <summary>
        /// Gets or sets electron stepx.
        /// </summary>
        public decimal StepX { get; set; }

        /// <summary>
        /// Gets or sets electron stepy.
        /// </summary>
        public decimal StepY { get; set; }
    }
}
