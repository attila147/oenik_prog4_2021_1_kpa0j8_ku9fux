﻿// <copyright file="GGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Gamemodel class.
    /// </summary>
    public class GGameModel : IGameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GGameModel"/> class.
        /// </summary>
        /// <param name="gameWidth">width.</param>
        /// <param name="gameHeight">height.</param>
        public GGameModel(double gameWidth, double gameHeight)
        {
            this.GameWidth = gameWidth;
            this.GameHeight = gameHeight;
        }

        /// <summary>
        /// Gets or sets components.
        /// </summary>
        public Component[] Components { get; set; }

        /// <summary>
        /// Gets or sets walls.
        /// </summary>
        public Wall[] Walls { get; set; }

        /// <summary>
        /// Gets game width.
        /// </summary>
        public double GameWidth { get; private set; }

        /// <summary>
        /// Gets game height.
        /// </summary>
        public double GameHeight { get; private set; }

        /// <summary>
        /// Gets or sets tile size.
        /// </summary>
        public double TileSize { get; set; }

        /// <summary>
        /// Gets or sets threads.
        /// </summary>
        public List<Thread> Threads { get; set; }

        /// <summary>
        /// Gets or sets act level.
        /// </summary>
        public string ActLevel { get; set; }

        /// <summary>
        /// Gets or sets player name.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets mode.
        /// </summary>
        public int Mode { get; set; }

        /// <summary>
        /// Gets or sets time.
        /// </summary>
        public TimeSpan Time { get; set; }
    }
}
