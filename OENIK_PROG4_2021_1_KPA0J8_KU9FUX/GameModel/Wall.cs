﻿// <copyright file="Wall.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Wall class.
    /// </summary>
    public class Wall
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Wall"/> class.
        /// </summary>
        /// <param name="point">point.</param>
        /// <param name="name">name.</param>
        public Wall(Point point, string name)
        {
            this.Point = point;
            this.Name = name;
        }

        /// <summary>
        /// Gets or sets wall point.
        /// </summary>
        public Point Point { get; set; }

        /// <summary>
        /// Gets or sets wall point.
        /// </summary>
        public string Name { get; set; }
    }
}
