﻿// <copyright file="Thread.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Thread class.
    /// </summary>
    public class Thread
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Thread"/> class.
        /// </summary>
        /// <param name="startingPoint">startpoint.</param>
        /// <param name="endPoint">endpoint.</param>
        /// <param name="owner">owner.</param>
        /// <param name="battle">battle.</param>
        public Thread(Point startingPoint, Point endPoint, string owner, bool battle = false)
        {
            this.StartingPoint = startingPoint;
            this.EndPoint = endPoint;
            this.Owner = owner;
            this.Battle = battle;
            this.EStepX = (decimal)(this.EndPoint.X - this.StartingPoint.X) / 200;
            this.EStepY = (decimal)(this.EndPoint.Y - this.StartingPoint.Y) / 200;
            this.Electrons = new LinkedList<Electron>();
        }

        /// <summary>
        /// Gets or sets starting point.
        /// </summary>
        public Point StartingPoint { get; set; }

        /// <summary>
        /// Gets or sets end point.
        /// </summary>
        public Point EndPoint { get; set; }

        /// <summary>
        /// Gets or sets owner.
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether battle.
        /// </summary>
        public bool Battle { get; set; }

        /// <summary>
        /// Gets or sets stepx.
        /// </summary>
        public decimal EStepX { get; set; }

        /// <summary>
        /// Gets or sets stepy.
        /// </summary>
        public decimal EStepY { get; set; }

        /// <summary>
        /// Gets or sets electrons.
        /// </summary>
        public LinkedList<Electron> Electrons { get; set; }
    }
}
