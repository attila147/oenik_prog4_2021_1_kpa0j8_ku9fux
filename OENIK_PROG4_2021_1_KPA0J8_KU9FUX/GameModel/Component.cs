﻿// <copyright file="Component.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameModel
{
    using System;
    using System.Windows;

    /// <summary>
    /// Component class.
    /// </summary>
    public class Component
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Component"/> class.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">name.</param>
        /// <param name="connectedIds">ids.</param>
        /// <param name="chargeLevel">charge.</param>
        /// <param name="point">point.</param>
        /// <param name="possibleThreads">possible threads.</param>
        /// <param name="actThread">actual threads.</param>
        /// <param name="owner">owner.</param>
        public Component(int id, string name, int[] connectedIds, int chargeLevel, Point point, int[] possibleThreads, int actThread = 0, string owner = "Neutral")
        {
            this.Id = id;
            this.Rect = new Rect(new Point(point.X, point.Y), new Size(40, 40));
            this.Name = name;
            this.Owner = owner;
            if (name == "CPU")
            {
                this.Capacity = 100;
                this.Production = 1;
                this.MaxThread = 2;
                this.Charging = 1;
            }
            else if (name == "GPU")
            {
                this.Capacity = 75;
                this.Production = 2;
                this.MaxThread = 3;
                this.Charging = 2;
            }
            else if (name == "ALU")
            {
                this.Capacity = 75;
                this.Production = 0;
                this.MaxThread = 4;
                this.Charging = 4;
            }
            else
            {
                this.Capacity = 200;
                this.Production = 1;
                this.MaxThread = 2;
                this.Charging = 1;
            }

            this.ActThread = actThread;
            this.ConnectedIds = connectedIds;
            this.ChargeLevel = chargeLevel;
            this.Point = point;
            this.PossibleThreadsIDS = possibleThreads;
        }

        /// <summary>
        /// Gets or sets component id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets component rect.
        /// </summary>
        public Rect Rect { get; set; }

        /// <summary>
        /// Gets or sets component name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets component owner.
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// Gets or sets component capacity.
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// Gets or sets component production.
        /// </summary>
        public int Production { get; set; }

        /// <summary>
        /// Gets or sets component charging.
        /// </summary>
        public int Charging { get; set; }

        /// <summary>
        /// Gets or sets component max thread.
        /// </summary>
        public int MaxThread { get; set; }

        /// <summary>
        /// Gets or sets component actual thread.
        /// </summary>
        public int ActThread { get; set; }

        /// <summary>
        /// Gets or sets component connected ids.
        /// </summary>
        public int[] ConnectedIds { get; set; }

        /// <summary>
        /// Gets or sets component charge level.
        /// </summary>
        public int ChargeLevel { get; set; }

        /// <summary>
        /// Gets or sets component point.
        /// </summary>
        public Point Point { get; set; }

        /// <summary>
        /// Gets or sets component possible threads.
        /// </summary>
        public int[] PossibleThreadsIDS { get; set; }
    }
}
