﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Unnesesery.>", Scope = "member", Target = "~M:CircuitWars.GameLogic.GGameLogic.InitModel(System.String)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<no time.>", Scope = "member", Target = "~M:CircuitWars.GameLogic.GGameLogic.Save")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<the list is fine here.>", Scope = "member", Target = "~M:CircuitWars.GameLogic.IGameLogic.GetLoadGames~System.Collections.Generic.List{System.String}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<the list is fine here.>", Scope = "member", Target = "~M:CircuitWars.GameLogic.IGameLogic.GetHighScores~System.Collections.Generic.List{CircuitWars.GameRepository.Result}")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "Uninteresting")]
