﻿// <copyright file="GGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Xml;
    using System.Xml.Linq;
    using CircuitWars.GameModel;
    using CircuitWars.GameRepository;

    /// <summary>
    /// The class which contains almost all gamelogic.
    /// </summary>
    public class GGameLogic : IGameLogic
    {
        private static IGameRepository gameRepository = new GGameRepo();
        private IGameModel gameModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="GGameLogic"/> class.
        /// </summary>
        /// <param name="gameModel">contains all data structurs.</param>
        /// <param name="fname">level name.</param>
        public GGameLogic(IGameModel gameModel, string fname)
        {
            this.gameModel = gameModel;
            if (!string.IsNullOrEmpty(fname))
            {
                this.InitModel(fname);
            }
            else
            {
                this.InitBasisModel();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GGameLogic"/> class.
        /// </summary>
        /// <param name="gameModel">gamodel instance.</param>
        public GGameLogic(IGameModel gameModel)
        {
            this.gameModel = gameModel;
        }

        /// <summary>
        /// Decidec if two lines intersect.
        /// </summary>
        /// <param name="p1">mouse down click x.</param>
        /// <param name="q1">mouse down click y.</param>
        /// <param name="p2">mouse up click x.</param>
        /// <param name="q2">mouse up click y.</param>
        /// <returns>true if they do.</returns>
        public static bool DoIntersect(Point p1, Point q1, Point p2, Point q2)
        {
            int o1 = Orientation(p1, q1, p2);
            int o2 = Orientation(p1, q1, q2);
            int o3 = Orientation(p2, q2, p1);
            int o4 = Orientation(p2, q2, q1);
            if (o1 != o2 && o3 != o4)
            {
                return true;
            }

            if (o1 == 0 && OnSegment(p1, p2, q1))
            {
                return true;
            }

            if (o2 == 0 && OnSegment(p1, q2, q1))
            {
                return true;
            }

            if (o3 == 0 && OnSegment(p2, p1, q2))
            {
                return true;
            }

            if (o4 == 0 && OnSegment(p2, q1, q2))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Makes a save in xml.
        /// </summary>
        public void Save()
        {
            int loadNumber = gameRepository.GetLoadNumber();
            string fname = $"{loadNumber}.xml";

            XDocument xdoc = new XDocument();
            XElement root = new XElement("game");
            XElement components = new XElement("components");
            xdoc.Add(root);
            root.Add(components);
            for (int i = 0; i < this.gameModel.Components.Length; i++)
            {
                XElement component = new XElement("component");
                components.Add(component);
                component.Add(new XAttribute("id", this.gameModel.Components[i].Id));
                component.Add(new XAttribute("name", this.gameModel.Components[i].Name));
                component.Add(new XAttribute("owner", this.gameModel.Components[i].Owner));
                component.Add(new XAttribute("actthread", this.gameModel.Components[i].ActThread));
                component.Add(new XAttribute("chargelevel", this.gameModel.Components[i].ChargeLevel));
                XElement point = new XElement("point");
                component.Add(point);
                point.Add(new XAttribute("y", this.gameModel.Components[i].Point.X));
                point.Add(new XAttribute("x", this.gameModel.Components[i].Point.Y));
                XElement connected = new XElement("connected");
                component.Add(connected);
                string data = string.Empty;
                foreach (var item in this.gameModel.Components[i].ConnectedIds)
                {
                    data += item + ",";
                }

                connected.Add(new XAttribute("connect", data));
                data = string.Empty;
                foreach (var item in this.gameModel.Components[i].PossibleThreadsIDS)
                {
                    data += item + ",";
                }

                XElement thread = new XElement("possible");
                component.Add(thread);
                thread.Add(new XAttribute("possible", data));
            }

            XElement walls = new XElement("walls");
            root.Add(walls);
            for (int i = 0; i < this.gameModel.Walls.Length; i++)
            {
                XElement wall = new XElement("wall");
                wall.Add(new XAttribute("y", this.gameModel.Walls[i].Point.X));
                wall.Add(new XAttribute("x", this.gameModel.Walls[i].Point.Y));
                wall.Add(new XAttribute("name", this.gameModel.Walls[i].Name));
                walls.Add(wall);
            }

            XElement threads = new XElement("threads");
            root.Add(threads);
            int id = 0;
            foreach (var item in this.gameModel.Threads)
            {
                XElement thread = new XElement("thread");
                threads.Add(thread);
                thread.Add(new XAttribute("id", ++id));
                thread.Add(new XAttribute("x1", (int)item.StartingPoint.X));
                thread.Add(new XAttribute("y1", (int)item.StartingPoint.Y));
                thread.Add(new XAttribute("x2", (int)item.EndPoint.X));
                thread.Add(new XAttribute("y2", (int)item.EndPoint.Y));
                thread.Add(new XAttribute("battle", item.Battle));
                thread.Add(new XAttribute("owner", item.Owner));
                XElement electrons = new XElement("electrons");
                thread.Add(electrons);
                foreach (var i in item.Electrons)
                {
                    XElement electron = new XElement("electron");
                    electron.Add(new XAttribute("id", id));
                    electron.Add(new XAttribute("x1", i.Point.X));
                    electron.Add(new XAttribute("y1", i.Point.X));
                    electron.Add(new XAttribute("actx", i.ActX));
                    electron.Add(new XAttribute("acty", i.ActY));
                    electron.Add(new XAttribute("stepx", i.StepX));
                    electron.Add(new XAttribute("stepy", i.StepY));
                    electron.Add(new XAttribute("charge", i.Charge));
                    electrons.Add(electron);
                }
            }

            XElement infos = new XElement("info");
            root.Add(infos);
            infos.Add(new XAttribute("name", this.gameModel.PlayerName));
            infos.Add(new XAttribute("level", this.gameModel.ActLevel));
            string modestring = string.Empty;
            if (this.gameModel.Mode == 3488)
            {
                modestring = "easy";
            }
            else if (this.gameModel.Mode == 2608)
            {
                modestring = "medium";
            }
            else if (this.gameModel.Mode == 1392)
            {
                modestring = "hard";
            }

            infos.Add(new XAttribute("mode", modestring));
            infos.Add(new XAttribute("time", this.gameModel.Time.ToString(@"mm\:ss")));

            xdoc.Save(fname);
            gameRepository.SaveGame(ref xdoc, this.gameModel.PlayerName);
        }

        /// <inheritdoc/>
        public void SaveHighScores(TimeSpan time)
        {
            gameRepository.SaveHighscores(this.gameModel.PlayerName, time, this.gameModel.ActLevel, this.gameModel.Mode);
        }

        /// <inheritdoc/>
        public List<Result> GetHighScores()
        {
            return gameRepository.GetHighScores();
        }

        /// <summary>
        /// Gets load games.
        /// </summary>
        /// <returns>a list of fnames.</returns>
        public List<string> GetLoadGames()
        {
            return gameRepository.GetLoadGames();
        }

        /// <inheritdoc/>
        public void ChargeMovement()
        {
            List<Thread> threads = this.gameModel.Threads;
            foreach (var thread in this.gameModel.Threads.ToList())
            {
                foreach (var value in thread.Electrons.ToList())
                {
                    value.ActX += value.StepX;
                    value.ActY += value.StepY;
                    Point point = new ((double)value.ActX, (double)value.ActY);
                    Rect rect = new ((double)value.ActX, (double)value.ActY, 2, 2);
                    value.Rect = rect;
                    value.Point = point;
                }
            }
        }

        /// <inheritdoc/>
        public void DeployElectrons()
        {
            foreach (var value in this.gameModel.Threads.ToList())
            {
                int componentid = this.SearchForComponentByPixel(value.StartingPoint);
                if (this.gameModel.Components[componentid].ChargeLevel >= this.gameModel.Components[componentid].Charging)
                {
                    Size size = new Size(1, 1);
                    decimal setX = (decimal)(value.EndPoint.X - value.StartingPoint.X) / 170;
                    decimal setY = (decimal)(value.EndPoint.Y - value.StartingPoint.Y) / 170;
                    decimal startPointX = (decimal)value.StartingPoint.X;
                    decimal startPointY = (decimal)value.StartingPoint.Y;
                    this.gameModel.Components[componentid].ChargeLevel -= this.gameModel.Components[componentid].Charging;
                    Electron electron = new Electron(value.Owner, value.StartingPoint, this.gameModel.Components[componentid].Charging, startPointX, startPointY, setX, setY);
                    value.Electrons.AddFirst(electron);
                }
            }
        }

        /// <inheritdoc/>
        public void ChargeIntersect()
        {
            for (int i = 0; i < this.gameModel.Components.Length; i++)
            {
                int changedsides = -1;
                Component charged = this.gameModel.Components[i];
                List<Thread> threads = this.gameModel.Threads;
                foreach (Thread thread in this.gameModel.Threads.ToList())
                {
                    if (thread.Electrons.Count != 0)
                    {
                        bool changedalready = false;
                        Electron electron = thread.Electrons.Last();
                        Rect rect = new Rect(this.gameModel.Components[i].Rect.X * this.gameModel.TileSize, this.gameModel.Components[i].Rect.Y * this.gameModel.TileSize, this.gameModel.TileSize, this.gameModel.TileSize);
                        Thread battlethread = this.gameModel.Threads.ToList().Where(x => x.EndPoint == thread.StartingPoint && x.StartingPoint == thread.EndPoint).FirstOrDefault();
                        if (battlethread != null && battlethread.Electrons.Count != 0)
                        {
                            Electron electronB = battlethread.Electrons.Last();
                            if (electron.Rect.IntersectsWith(electronB.Rect))
                            {
                                if (electron.Charge == electronB.Charge)
                                {
                                    thread.Electrons.Remove(electron);
                                    changedalready = true;
                                    this.gameModel.Threads.Where(x => x.EndPoint == thread.StartingPoint && x.StartingPoint == thread.EndPoint).FirstOrDefault().Electrons.Remove(electronB);
                                }
                                else if (electron.Charge > electronB.Charge)
                                {
                                    thread.Electrons.Last().Charge -= electronB.Charge;
                                    this.gameModel.Threads.Where(x => x.EndPoint == thread.StartingPoint && x.StartingPoint == thread.EndPoint).FirstOrDefault().Electrons.RemoveLast();
                                }
                                else
                                {
                                    this.gameModel.Threads.Where(x => x.EndPoint == thread.StartingPoint && x.StartingPoint == thread.EndPoint).FirstOrDefault().Electrons.Last().Charge -= electron.Charge;
                                    thread.Electrons.RemoveLast();
                                    changedalready = true;
                                }
                            }
                        }

                        if (this.gameModel.Components[i].Point != this.PixelToTileConverter(thread.StartingPoint) && electron.Rect.IntersectsWith(rect) && !changedalready)
                        {
                            if (electron.Owner == charged.Owner)
                            {
                                charged.ChargeLevel += electron.Charge;
                                thread.Electrons.Remove(electron);
                            }
                            else if (electron.Owner != charged.Owner && charged.Owner != "Neutral")
                            {
                                if ((charged.ChargeLevel - electron.Charge) >= 0)
                                {
                                    charged.ChargeLevel -= electron.Charge;
                                    thread.Electrons.Remove(electron);
                                }
                                else
                                {
                                    charged.Owner = electron.Owner;
                                    changedsides = charged.Id;
                                    charged.ChargeLevel = Math.Abs(charged.ChargeLevel - electron.Charge);
                                    thread.Electrons.Remove(electron);
                                }
                            }
                            else
                            {
                                charged.Owner = electron.Owner;
                                charged.ChargeLevel += electron.Charge;
                                thread.Electrons.Remove(electron);
                            }
                        }
                    }
                }

                this.gameModel.Threads = threads;
                if (changedsides != -1)
                {
                    this.ThreadRemoveByComponent(changedsides);
                }
            }
        }

        /// <inheritdoc/>
        public void ComponentProductionTick()
        {
            for (int i = 0; i < this.gameModel.Components.Length; i++)
            {
                if ((this.gameModel.Components[i].ChargeLevel + this.gameModel.Components[i].Production) < this.gameModel.Components[i].Capacity && this.gameModel.Components[i].Owner != "Neutral")
                {
                    this.gameModel.Components[i].ChargeLevel += this.gameModel.Components[i].Production;
                }
                else if (this.gameModel.Components[i].Owner != "Neutral")
                {
                    this.gameModel.Components[i].ChargeLevel = this.gameModel.Components[i].Capacity;
                }
            }
        }

        /// <inheritdoc/>
        public void ComponentTick()
        {
            this.ComponentProductionTick();
        }

        /// <inheritdoc/>
        public bool PossibleThread(Point pointA, Point pointB)
        {
            Component a = this.gameModel.Components[this.SearchForComponentByPixel(pointA)];
            Component b = this.gameModel.Components[this.SearchForComponentByPixel(pointB)];
            int k = 0;
            bool exitsAlready = false;
            while (k < this.gameModel.Threads.Count && !(this.PixelToTileConverter(this.gameModel.Threads[k].StartingPoint) == a.Point && this.PixelToTileConverter(this.gameModel.Threads[k].EndPoint) == b.Point))
            {
                k++;
            }

            if (k < this.gameModel.Threads.Count)
            {
                exitsAlready = true;
            }

            int i = 0;
            while (i < a.PossibleThreadsIDS.Length && (a.PossibleThreadsIDS[i] != b.Id))
            {
                i++;
            }

            if (i < a.PossibleThreadsIDS.Length && !exitsAlready)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the position of the Component in the big table.
        /// </summary>
        /// <param name="point">Pixel where the user clicked.</param>
        /// <returns>Position of the Component.</returns>
        public int SearchForComponentByPixel(Point point)
        {
            Point tilePoint = this.PixelToTileConverter(point);
            int i = 0;
            while (i < this.gameModel.Components.Length && !(this.gameModel.Components[i].Point.X == tilePoint.X && this.gameModel.Components[i].Point.Y == tilePoint.Y) && this.StillGoing())
            {
                i++;
            }

            if (i < this.gameModel.Components.Length)
            {
                return i;
            }
            else
            {
                return -1;
            }
        }

        /// <inheritdoc/>
        public Point PixelToTileConverter(Point point)
        {
            Point tile = new Point(-1, -1);
            tile.X = (int)(point.X / this.gameModel.TileSize);
            tile.Y = (int)(point.Y / this.gameModel.TileSize);
            return tile;
        }

        /// <inheritdoc/>
        public bool StillGoing()
        {
            if (this.gameModel.Components.Any(x => x.Owner == "AI") && this.gameModel.Components.Any(x => x.Owner == "player"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// gets who won.
        /// </summary>
        /// <returns>true if player.</returns>
        public bool Whowon()
        {
            if (this.gameModel.Components.Any(x => x.Owner == "player"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public void ThreadInitiate(int chargerComponentsNumber, int chargedComponentsNumber)
        {
            if (this.gameModel.Components[chargedComponentsNumber].ConnectedIds.Contains(chargerComponentsNumber) && this.gameModel.Components[chargedComponentsNumber].Owner == this.gameModel.Components[chargerComponentsNumber].Owner)
            {
                this.AiThreadRemove(this.gameModel.Components[chargedComponentsNumber].Point, this.gameModel.Components[chargerComponentsNumber].Point);
            }

            int i = 0;
            bool done = false;
            bool battle = false;
            int index = -1;
            while (i < this.gameModel.Components[chargerComponentsNumber].ConnectedIds.Length && !done)
            {
                if (this.gameModel.Components[chargerComponentsNumber].ConnectedIds[i] == -1)
                {
                    if (this.gameModel.Components[chargedComponentsNumber].ConnectedIds.Contains(chargerComponentsNumber) && this.gameModel.Components[chargedComponentsNumber].Owner != this.gameModel.Components[chargerComponentsNumber].Owner)
                    {
                        battle = true;
                        index = this.gameModel.Threads.FindIndex(x => this.PixelToTileConverter(x.StartingPoint) == this.gameModel.Components[chargedComponentsNumber].Point && this.PixelToTileConverter(x.EndPoint) == this.gameModel.Components[chargerComponentsNumber].Point);
                    }

                    this.gameModel.Components[chargerComponentsNumber].ConnectedIds[i] = this.gameModel.Components[chargedComponentsNumber].Id;
                    this.gameModel.Components[chargerComponentsNumber].ActThread++;
                    Thread thread = new Thread(this.TileToPixelConverter(this.gameModel.Components[chargerComponentsNumber].Point), this.TileToPixelConverter(this.gameModel.Components[chargedComponentsNumber].Point), this.gameModel.Components[chargerComponentsNumber].Owner);
                    thread.Battle = battle;
                    this.gameModel.Threads.Add(thread);
                    if (index >= 0)
                    {
                        this.gameModel.Threads[index].Battle = true;
                    }

                    done = true;
                }

                i++;
            }
        }

        /// <inheritdoc/>
        public void PlayerThreadRemove(Point pointA, Point pointB)
        {
            List<Thread> removables = new List<Thread>();
            List<int> threadindex = new List<int>();
            foreach (Thread thread in this.gameModel.Threads.ToList())
            {
                if (thread.Owner == "player" && DoIntersect(pointA, pointB, thread.StartingPoint, thread.EndPoint))
                {
                    int id = this.gameModel.Components.Where(x => x.Point == this.PixelToTileConverter(thread.StartingPoint)).FirstOrDefault().Id;
                    int id2 = this.gameModel.Components.Where(x => x.Point == this.PixelToTileConverter(thread.EndPoint)).FirstOrDefault().Id;
                    int i = 0;
                    while (this.gameModel.Components[id].ConnectedIds[i] != id2)
                    {
                        i++;
                    }

                    if (this.gameModel.Components.Any(x => x.ConnectedIds.Contains(id) && x.Id != id2))
                    {
                        int j = this.gameModel.Threads.FindIndex(x => this.PixelToTileConverter(x.StartingPoint) == this.gameModel.Components[id2].Point && this.PixelToTileConverter(x.EndPoint) == this.gameModel.Components[id].Point);
                        if (j != -1)
                        {
                            threadindex.Add(j);
                        }
                    }

                    int returncharge = 0;
                    if (thread.Electrons.Count > 0)
                    {
                        foreach (var item in thread.Electrons)
                        {
                            returncharge += item.Charge;
                        }
                    }

                    int index = i;
                    this.gameModel.Components[id].ConnectedIds[index] = -1;
                    this.gameModel.Components[id].ActThread--;
                    this.gameModel.Components[id].ChargeLevel += returncharge;
                    removables.Add(thread);
                }
            }

            this.ChangeBattleStatus(threadindex);
            foreach (var item in removables)
            {
                this.gameModel.Threads.Remove(item);
            }
        }

        /// <inheritdoc/>
        public void UserInputControl(Point pointA, Point pointB)
        {
            int componentA = this.SearchForComponentByPixel(pointA);
            int componentB = this.SearchForComponentByPixel(pointB);
            if (componentA != -1 && componentB != -1 && this.gameModel.Components[componentA].Owner == "player")
            {
                if (this.PossibleThread(pointA, pointB))
                {
                    this.ThreadInitiate(componentA, componentB);
                }
            }
            else if (componentA == -1)
            {
                this.PlayerThreadRemove(pointA, pointB);
            }
        }

        /// <summary>
        /// gets a bool true if attacked.
        /// </summary>
        /// <param name="id">components id.</param>
        /// <returns>true if yes.</returns>
        public bool GetIsAttacked(int id)
        {
            foreach (var i in this.gameModel.Components.ToList())
            {
                int k = 0;
                while (k < i.ConnectedIds.Length)
                {
                    if (i.ConnectedIds[k] == id && i.Owner != this.gameModel.Components[id].Owner)
                    {
                        return true;
                    }

                    k++;
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public void AiTick()
        {
            Component aIcomp;
            List<ComponentShadow> shadows = new List<ComponentShadow>();
            Component helper;
            int[] newThreads;
            int counter = 0;
            int threadcharges = 0;
            int[] oldThreads;
            foreach (var component in this.gameModel.Components.ToList())
            {
                if (component.Owner == "AI")
                {
                    shadows = new List<ComponentShadow>();
                    aIcomp = component;
                    oldThreads = aIcomp.ConnectedIds;
                    newThreads = new int[aIcomp.PossibleThreadsIDS.Length];
                    newThreads = FillUp(newThreads);
                    foreach (var item in this.gameModel.Threads)
                    {
                        if (this.PixelToTileConverter(item.StartingPoint) == aIcomp.Point)
                        {
                            foreach (var electron in item.Electrons)
                            {
                                threadcharges += electron.Charge;
                            }
                        }
                    }

                    for (int i = 0; i < aIcomp.PossibleThreadsIDS.Length; i++)
                    {
                        helper = this.gameModel.Components[aIcomp.PossibleThreadsIDS[i]];
                        ComponentShadow componentShadow = new ComponentShadow(helper.Id, helper.Owner, helper.ConnectedIds, helper.ChargeLevel, this.GetIsAttacked(helper.Id), this.gameModel.Components[helper.Id].ConnectedIds.Contains(aIcomp.Id), this.gameModel.Components[aIcomp.Id].ConnectedIds.Contains(helper.Id));
                        shadows.Add(componentShadow);
                    }

                    if (shadows.Any(x => x.Owner == "Neutral"))
                    {
                        foreach (var shadow in shadows)
                        {
                            if (shadow.Owner == "Neutral" && counter < aIcomp.MaxThread)
                            {
                                newThreads[counter] = shadow.Id;
                                counter++;
                            }
                        }
                    }
                    else
                    {
                        if (shadows.Any(x => x.ConnectedTOus && x.Owner != aIcomp.Owner))
                        {
                            int num = shadows.Where(x => x.ConnectedTOus).FirstOrDefault().Id;
                            newThreads[0] = num;
                            counter++;
                            if (shadows.Any(x => x.Owner != aIcomp.Owner && aIcomp.ChargeLevel - x.ChargeLevel > 2))
                            {
                                num = shadows.Where(x => x.Owner != aIcomp.Owner && aIcomp.ChargeLevel - x.ChargeLevel > 2).FirstOrDefault().Id;
                                newThreads[counter] = num;
                                counter++;
                            }

                            if (counter < newThreads.Length && shadows.Any(x => x.Owner == aIcomp.Owner && aIcomp.ChargeLevel - 2 > x.ChargeLevel))
                            {
                                num = shadows.Where(x => x.Owner == aIcomp.Owner && aIcomp.ChargeLevel - 2 > x.ChargeLevel).FirstOrDefault().Id;
                                newThreads[counter] = num;
                                counter++;
                            }
                        }
                        else if (shadows.Any(x => x.IsAttacked && x.Owner == aIcomp.Owner))
                        {
                            int allowed = aIcomp.ChargeLevel > 5 ? allowed = newThreads.Length : allowed = 1;
                            if (allowed > 1)
                            {
                                int num = shadows.Where(x => x.IsAttacked).FirstOrDefault().Id;
                                newThreads[counter] = num;
                                counter++;
                                while (counter < allowed)
                                {
                                    foreach (var item in shadows)
                                    {
                                        if (item.Owner == aIcomp.Owner && item.Id != num && counter < allowed)
                                        {
                                            newThreads[counter] = item.Id;
                                            counter++;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                int num = shadows.Where(x => x.IsAttacked).FirstOrDefault().Id;
                                newThreads[counter] = num;
                                counter++;
                            }
                        }
                        else if (shadows.Any(x => x.Owner != aIcomp.Owner && aIcomp.ChargeLevel + threadcharges - x.ChargeLevel > 2))
                        {
                            int allowed = aIcomp.ChargeLevel + threadcharges > 10 ? allowed = newThreads.Length : allowed = 1;
                            foreach (var item in shadows)
                            {
                                if (item.Owner != aIcomp.Owner && item.ChargeLevel - 10 < aIcomp.ChargeLevel && aIcomp.ChargeLevel > 2 && counter < allowed)
                                {
                                    newThreads[counter] = item.Id;
                                    counter++;
                                }
                            }
                        }
                        else
                        {
                            foreach (var item in shadows)
                            {
                                if (item.ChargeLevel < aIcomp.ChargeLevel && aIcomp.ChargeLevel > 5 && counter < newThreads.Length)
                                {
                                    newThreads[counter] = item.Id;
                                    counter++;
                                }
                            }
                        }
                    }

                    foreach (var iDtoThread in oldThreads)
                    {
                        if (iDtoThread != -1 && !newThreads.Contains(iDtoThread))
                        {
                            this.AiThreadRemove(this.gameModel.Components[aIcomp.Id].Point, this.gameModel.Components[iDtoThread].Point);
                        }
                    }

                    foreach (var iDtoThread in newThreads)
                    {
                        if (iDtoThread != -1 && !oldThreads.Contains(iDtoThread))
                        {
                            this.ThreadInitiate(aIcomp.Id, iDtoThread);
                        }
                    }

                    aIcomp.ActThread = newThreads.Where(x => x != -1).Count();
                    counter = 0;
                }
            }
        }

        private static int[] FillUp(int[] thread)
        {
            for (int i = 0; i < thread.Length; i++)
            {
                thread[i] = -1;
            }

            return thread;
        }

        private static bool OnSegment(Point p, Point q, Point r)
        {
            if (q.X <= Math.Max(p.X, r.X) && q.X >= Math.Min(p.X, r.X) &&
                q.Y <= Math.Max(p.Y, r.Y) && q.Y >= Math.Min(p.Y, r.Y))
            {
                return true;
            }

            return false;
        }

        private static int Orientation(Point p, Point q, Point r)
        {
            int val = (int)(((q.Y - p.Y) * (r.X - q.X)) - ((q.X - p.X) * (r.Y - q.Y)));
            if (val == 0)
            {
                return 0;
            }

            return (val > 0) ? 1 : 2;
        }

        private void ThreadRemoveByComponent(int removable)
        {
            List<int> threadindex = new List<int>();
            List<Thread> removables = new List<Thread>();
            foreach (Thread thread in this.gameModel.Threads.ToList())
            {
                if (this.gameModel.Components[removable].Point == this.PixelToTileConverter(thread.StartingPoint))
                {
                    int id = removable;
                    int id2 = this.gameModel.Components.Where(x => x.Point == this.PixelToTileConverter(thread.EndPoint)).FirstOrDefault().Id;
                    int i = 0;
                    while (i < this.gameModel.Components[id].ConnectedIds.Length && this.gameModel.Components[id].ConnectedIds[i] != id2)
                    {
                        i++;
                    }

                    if (this.gameModel.Components.Any(x => x.ConnectedIds.Contains(id) && x.Id != id))
                    {
                        int j = this.gameModel.Threads.FindIndex(x => this.PixelToTileConverter(x.StartingPoint) == this.gameModel.Components[id2].Point && this.PixelToTileConverter(x.EndPoint) == this.gameModel.Components[id].Point);
                        if (j != -1)
                        {
                            threadindex.Add(j);
                        }
                    }

                    if (i < this.gameModel.Components[id].ConnectedIds.Length)
                    {
                        int index = i;
                        this.gameModel.Components[id].ConnectedIds[index] = -1;
                        this.gameModel.Components[id].ActThread = 0;
                        removables.Add(thread);
                    }
                }
            }

            this.ChangeBattleStatus(threadindex);
            foreach (var item in removables)
            {
                this.gameModel.Threads.Remove(item);
            }
        }

        private void ChangeBattleStatus(List<int> threadindex)
        {
            foreach (var item in threadindex)
            {
                this.gameModel.Threads[item].Battle = false;
            }
        }

        private void AiThreadRemove(Point pointA, Point pointB)
        {
            List<int> threadindex = new List<int>();
            List<Thread> removables = new List<Thread>();
            foreach (Thread thread in this.gameModel.Threads.ToList())
            {
                if (this.PixelToTileConverter(thread.StartingPoint) == pointA && this.PixelToTileConverter(thread.EndPoint) == pointB)
                {
                    int id = this.gameModel.Components.Where(x => x.Point == this.PixelToTileConverter(thread.StartingPoint)).FirstOrDefault().Id;
                    int id2 = this.gameModel.Components.Where(x => x.Point == this.PixelToTileConverter(thread.EndPoint)).FirstOrDefault().Id;
                    int i = 0;
                    while (this.gameModel.Components[id].ConnectedIds[i] != id2)
                    {
                        i++;
                    }

                    if (this.gameModel.Components.Any(x => x.ConnectedIds.Contains(id) && x.Id != id2))
                    {
                        int j = this.gameModel.Threads.FindIndex(x => this.PixelToTileConverter(x.StartingPoint) == this.gameModel.Components[id2].Point && this.PixelToTileConverter(x.EndPoint) == this.gameModel.Components[id].Point);
                        if (j != -1)
                        {
                            threadindex.Add(j);
                        }
                    }

                    int returncharge = 0;
                    if (thread.Electrons.Count > 0)
                    {
                        foreach (var item in thread.Electrons)
                        {
                            returncharge += item.Charge;
                        }
                    }

                    int index = i;
                    this.gameModel.Components[id].ConnectedIds[index] = -1;
                    this.gameModel.Components[id].ActThread--;
                    this.gameModel.Components[id].ChargeLevel += returncharge;
                    removables.Add(thread);
                }
            }

            this.ChangeBattleStatus(threadindex);
            foreach (var item in removables.ToList())
            {
                this.gameModel.Threads.Remove(item);
            }
        }

        private void InitModel(string fname)
        {
            XDocument xdoc = XDocument.Load(fname);
            var comps = (from x in xdoc.Descendants("component")
                         select new
                         {
                             id = int.Parse(x.Attribute("id").Value),
                             name = x.Attribute("name").Value,
                             owner = x.Attribute("owner").Value,
                             actThread = int.Parse(x.Attribute("actthread").Value),
                             chargeLevel = int.Parse(x.Attribute("chargelevel").Value),
                             x = x.Element("point").Attribute("y").Value,
                             y = x.Element("point").Attribute("x").Value,
                             connected = x.Element("connected").Attribute("connect").Value,
                             possible = x.Element("possible").Attribute("possible").Value,
                         }).ToList();

            var wall = (from x in xdoc.Descendants("wall")
                        select new
                        {
                            x = x.Attribute("y").Value,
                            y = x.Attribute("x").Value,
                            name = x.Attribute("name").Value,
                        }).ToList();
            var thread = from x in xdoc.Descendants("thread")
                         select new
                         {
                             id = x.Attribute("id").Value,
                             x1 = x.Attribute("x1").Value,
                             y1 = x.Attribute("y1").Value,
                             x2 = x.Attribute("x2").Value,
                             y2 = x.Attribute("y2").Value,
                             battle = x.Attribute("battle").Value,
                             owner = x.Attribute("owner").Value,
                         };
            var electron = from x in xdoc.Descendants("electron")
                           select new
                           {
                               id = x.Attribute("id").Value,
                               x1 = x.Attribute("x1").Value,
                               y1 = x.Attribute("y1").Value,
                               actx = x.Attribute("actx").Value,
                               acty = x.Attribute("acty").Value,
                               stepx = x.Attribute("stepx").Value,
                               stepy = x.Attribute("stepy").Value,
                               charge = x.Attribute("charge").Value,
                           };
            var infos = from x in xdoc.Descendants("info")
                        select new
                        {
                            name = x.Attribute("name").Value,
                            level = x.Attribute("level").Value,
                            mode = x.Attribute("mode").Value,
                            time = x.Attribute("time").Value,
                        };
            Component[] components = new Component[comps.Count];
            int idx = 0;
            foreach (var i in comps)
            {
                string numbers = i.possible;
                string[] splits = numbers.Split(',');
                int[] pthreads = new int[splits.Length - 1];
                for (int j = 0; j < pthreads.Length; j++)
                {
                    pthreads[j] = int.Parse(splits[j]);
                }

                string connect = i.connected;
                string[] splits2 = connect.Split(',');
                int[] connectId = new int[splits2.Length - 1];
                for (int j = 0; j < connectId.Length; j++)
                {
                    connectId[j] = int.Parse(splits2[j]);
                }

                Point point = new Point(double.Parse(i.x), double.Parse(i.y));
                components[idx] = new Component(i.id, i.name, connectId, i.chargeLevel, point, pthreads, i.actThread, i.owner);
                idx++;
            }

            this.gameModel.Components = components;
            Wall[] walls = new Wall[wall.Count];
            idx = 0;
            foreach (var i in wall)
            {
                walls[idx] = new Wall(new Point(double.Parse(i.x), double.Parse(i.y)), i.name);
                idx++;
            }

            this.gameModel.Walls = walls;
            if (this.gameModel.GameWidth > 1800 && this.gameModel.GameWidth < 2500)
            {
                this.gameModel.TileSize = Math.Round(this.gameModel.GameWidth / 16);
            }
            else if (this.gameModel.GameWidth > 2550 && this.gameModel.GameWidth < 3600)
            {
                this.gameModel.TileSize = Math.Round(this.gameModel.GameWidth / 16);
            }
            else if (this.gameModel.GameWidth > 3600 && this.gameModel.GameWidth < 5000)
            {
                this.gameModel.TileSize = Math.Round(this.gameModel.GameWidth / 26);
            }
            else if (this.gameModel.GameWidth > 900 && this.gameModel.GameWidth < 1800)
            {
                this.gameModel.TileSize = Math.Round(this.gameModel.GameWidth / 16);
            }

            List<Thread> threads = new List<Thread>();
            foreach (var i in thread)
            {
                Point start = new Point(double.Parse(i.x1), double.Parse(i.y1));
                Point end = new Point(double.Parse(i.x2), double.Parse(i.y2));
                LinkedList<Electron> electrons = new LinkedList<Electron>();
                foreach (var it in electron)
                {
                    if (i.id == it.id)
                    {
                        Point epoint = new Point(XmlConvert.ToDouble(it.x1), XmlConvert.ToDouble(it.y1));
                        Electron e = new Electron(i.owner, epoint, int.Parse(it.charge), XmlConvert.ToDecimal(it.actx), XmlConvert.ToDecimal(it.acty), XmlConvert.ToDecimal(it.stepx), XmlConvert.ToDecimal(it.stepy));
                        electrons.AddLast(e);
                    }
                }

                Thread t = new Thread(start, end, i.owner, bool.Parse(i.battle));
                t.Electrons = electrons;
                threads.Add(t);
            }

            this.gameModel.Threads = threads;

            foreach (var item in infos)
            {
                this.gameModel.PlayerName = item.name;
                this.gameModel.ActLevel = item.level;
                if (item.mode == "easy")
                {
                    this.gameModel.Mode = 3488;
                }
                else if (item.mode == "medium")
                {
                    this.gameModel.Mode = 2608;
                }
                else if (item.mode == "hard")
                {
                    this.gameModel.Mode = 1392;
                }

                this.gameModel.Time = TimeSpan.Parse(item.time);
            }
        }

        private Point TileToPixelConverter(Point point)
        {
            point.X = (point.X * this.gameModel.TileSize) + (this.gameModel.TileSize / 2);
            point.Y = (point.Y * this.gameModel.TileSize) + (this.gameModel.TileSize / 2);
            return point;
        }

        private void InitBasisModel()
        {
            this.gameModel.Components = new Component[1];
            this.gameModel.Threads = new List<Thread>();
            this.gameModel.Threads.Add(null);
            this.gameModel.Walls = new Wall[1];
        }
    }
}
