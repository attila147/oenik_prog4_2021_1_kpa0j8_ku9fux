﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using CircuitWars.GameModel;
    using CircuitWars.GameRepository;

    /// <summary>
    /// Interface for game logic, including all public methods.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// This method decides if the match is over.
        /// </summary>
        /// <returns>True if its still going.</returns>
        bool StillGoing();

        /// <summary>
        /// Gets a components id by pixel point.
        /// </summary>
        /// <param name="point">pixel.</param>
        /// <returns>a component id.</returns>
        int SearchForComponentByPixel(Point point);

        /// <summary>
        /// Decides which action to take by clicks.
        /// </summary>
        /// <param name="pointA">mouse press down pixel.</param>
        /// <param name="pointB">mouse up pixel.</param>
        void UserInputControl(Point pointA, Point pointB);

        /// <summary>
        /// Makes the calculation from pixel to tile.
        /// </summary>
        /// <param name="point">pixel.</param>
        /// <returns>a tile point.</returns>
        Point PixelToTileConverter(Point point);

        /// <summary>
        /// Makes electrons move one step.
        /// </summary>
        void ChargeMovement();

        /// <summary>
        /// Deploys electrons at the start of threads.
        /// </summary>
        public void DeployElectrons();

        /// <summary>
        /// Makes it possible for electrons and components to colide.
        /// </summary>
        void ChargeIntersect();

        /// <summary>
        /// Decides if it is allowed by the map to make that thread.
        /// </summary>
        /// <param name="pointA">first components tile.</param>
        /// <param name="pointB">second components tile.</param>
        /// <returns>true if possible.</returns>
        bool PossibleThread(Point pointA, Point pointB);

        /// <summary>
        /// Adds each components production value to itself.
        /// </summary>
        void ComponentProductionTick();

        /// <summary>
        /// Makes it possible for the user to create a thread.
        /// </summary>
        /// <param name="chargerComponentsNumber">first componets id.</param>
        /// <param name="chargedComponentsNumber">second componets id.</param>
        void ThreadInitiate(int chargerComponentsNumber, int chargedComponentsNumber);

        /// <summary>
        /// Removes a thread and changes the gamemodel.
        /// </summary>
        /// <param name="pointA">Mouse down point.</param>
        /// <param name="pointB">Mouse up point.</param>
        void PlayerThreadRemove(Point pointA, Point pointB);

        /// <summary>
        /// USELESS.
        /// </summary>
        void ComponentTick();

        /// <summary>
        /// Makes decisions as the oposite sides.
        /// </summary>
        void AiTick();

        /// <summary>
        /// Gets the winner.
        /// </summary>
        /// <returns>true if player.</returns>
        bool Whowon();

        /// <summary>
        /// Saves the gamemodel to xml.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets the nemes.
        /// </summary>
        /// <returns>Gets the name of the xmls.</returns>
        List<string> GetLoadGames();

        /// <summary>
        /// Saves high score.
        /// </summary>
        /// <param name="time">the score is the time.</param>
        public void SaveHighScores(TimeSpan time);

        /// <summary>
        /// Gets the high scores.
        /// </summary>
        /// <returns>list of results.</returns>
        List<Result> GetHighScores();
    }
}
