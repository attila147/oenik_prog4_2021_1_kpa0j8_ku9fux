﻿// <copyright file="ModelSetup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameLogicTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using CircuitWars.GameModel;

    /// <summary>
    /// Modell setup class for tests.
    /// </summary>
    public static class ModelSetup
    {
        /// <summary>
        /// Model for tests.
        /// </summary>
        /// <returns>Returns with a model.</returns>
        public static IGameModel Model()
        {
            IGameModel gamemodel = new GGameModel(1400, 900);
            gamemodel.TileSize = 100;
            gamemodel.Threads = new List<Thread>();
            gamemodel.Walls = new Wall[2];
            int[] player0connected = new int[] { 1, 2 };
            int[] player0possible = new int[] { 1, 2, 3 };
            Point pointp0 = new Point(1, 1);
            int[] neutral0connected = new int[] { -1, -1 };
            int[] neutral0possible = new int[] { 0, 2, 3 };
            Point neutral0 = new Point(3, 1);
            int[] neutral1connected = new int[] { -1, -1 };
            int[] neutral1possible = new int[] { 1, 0, 3 };
            Point neutral1 = new Point(1, 5);
            int[] ai0connected = new int[] { 0, 2 };
            int[] ai0possible = new int[] { 1, 2, 0 };
            Point ai0 = new Point(3, 5);
            gamemodel.Components = new Component[]
            {
                new Component(0, "CPU", player0connected, 20, pointp0, player0possible, 1, "player"),
                new Component(1, "CPU", neutral0connected, 20, neutral0, neutral0possible, 0, "Neutral"),
                new Component(2, "CPU", neutral1connected, 20, neutral1, neutral1possible, 0, "Neutral"),
                new Component(3, "CPU", ai0connected, 20, ai0, ai0possible, 1, "AI"),
            };
            Thread thread0 = new Thread(new Point(150, 150), new Point(350, 550), "player");
            Thread thread1 = new Thread(new Point(350, 550), new Point(150, 150), "AI");
            gamemodel.Threads.Add(thread0);
            gamemodel.Threads.Add(thread1);
            return gamemodel;
        }
    }
}
