﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameLogicTests
{
    using System.Collections.Generic;
    using System.Windows;
    using CircuitWars.GameLogic;
    using CircuitWars.GameModel;
    using NUnit.Framework;

    /// <summary>
    /// Logic tests for the game.
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        /// <summary>
        /// Gets a true if a thread goes to that.
        /// </summary>
        [Test]
        public void TestGetIsAttacked()
        {
            IGameModel gamemodel = ModelSetup.Model();
            GGameLogic gamelogic = new GGameLogic(gamemodel);
            bool expectedresult = true;
            var result = gamelogic.GetIsAttacked(gamemodel.Components[0].Id);
            Assert.That(result, Is.EqualTo(expectedresult));
        }

        /// <summary>
        /// Gets a component id by pixel.
        /// </summary>
        [Test]
        public void TestSearchforComponentbyPixel()
        {
            IGameModel gamemodel = ModelSetup.Model();
            GGameLogic gameLogic = new GGameLogic(gamemodel);
            int expectedresult = 3;
            var result = gameLogic.SearchForComponentByPixel(new System.Windows.Point(330, 521));
            Assert.That(result, Is.EqualTo(expectedresult));
        }

        /// <summary>
        /// deploys electrons on threads.
        /// </summary>
        [Test]
        public void TestDeployElectrons()
        {
            IGameModel gamemodel = ModelSetup.Model();
            IGameModel gamemodel2 = ModelSetup.Model();
            GGameLogic gameLogic = new GGameLogic(gamemodel);
            Electron electron0 = new Electron("player", new System.Windows.Point(150, 150), 1, 150, 150, 1, 2);
            Electron electron1 = new Electron("AI", new System.Windows.Point(350, 550), 1, 350, 550, -1, -2);
            List<Thread> threads = gamemodel2.Threads;
            threads[0].Electrons.AddLast(electron0);
            threads[1].Electrons.AddLast(electron1);
            gamemodel2.Threads = threads;
            gamemodel2.Components[0].ChargeLevel--;
            gamemodel2.Components[3].ChargeLevel--;
            gameLogic.DeployElectrons();
            Assert.That(gamemodel.Threads[0].Electrons.Count, Is.EqualTo(gamemodel2.Threads[0].Electrons.Count));
            Assert.That(gamemodel.Components[0].ChargeLevel, Is.EqualTo(gamemodel2.Components[0].ChargeLevel));
        }

        /// <summary>
        /// Test for remove thread helper method.
        /// </summary>
        /// <param name="ax">fist point x.</param>
        /// <param name="ay">fist point y.</param>
        /// <param name="bx">second point x.</param>
        /// <param name="by">secont point y.</param>
        /// <param name="cx">third point x.</param>
        /// <param name="cy">third point y.</param>
        /// <param name="dx">fourth point x.</param>
        /// <param name="dy">fourth point y.</param>
        /// <param name="e">if its true.</param>
        [Test]
        [TestCase(1, 1, 3, 3, 1, 3, 3, 1, true)]
        [TestCase(1, 1, 3, 3, 4, 5, 6, 7, false)]
        [TestCase(1, 4, 2, 3, 1, 3, 3, 1, false)]
        [TestCase(1, 2, 0, 3, 4, 6, 4, 7, false)]
        public void TestDoIntersect(int ax, int ay, int bx, int by, int cx, int cy, int dx, int dy, bool e)
        {
            IGameModel gamemodel = ModelSetup.Model();
            GGameLogic gameLogic = new GGameLogic(gamemodel);
            bool dothey = GGameLogic.DoIntersect(new Point(ax, ay), new Point(bx, by), new Point(cx, cy), new Point(dx, dy));
            Assert.That(dothey, Is.EqualTo(e));
        }

        /// <summary>
        /// Testing if its incrementinc all active components by their production.
        /// </summary>
        /// <param name="a">components id.</param>
        [Test]
        [TestCase(0)]
        [TestCase(3)]
        public void TestComponentProductionTick(int a)
        {
            IGameModel gamemodel = ModelSetup.Model();
            IGameModel gamemodel2 = ModelSetup.Model();
            GGameLogic gameLogic = new GGameLogic(gamemodel);
            gameLogic.ComponentProductionTick();
            Assert.That(gamemodel.Components[a].ChargeLevel, Is.EqualTo(gamemodel2.Components[a].ChargeLevel + gamemodel2.Components[a].Production));
        }
    }
}
