﻿// <copyright file="HighScoreRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using CircuitWars.GameModel;
    using CircuitWars.GameRepository;

    /// <summary>
    /// Highscore renderer class.
    /// </summary>
    public class HighScoreRenderer
    {
        private static IGameModel model;
        private StackPanel stackPanel = new StackPanel();

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreRenderer"/> class.
        /// </summary>
        /// <param name="gameModel">game model.</param>
        public HighScoreRenderer(IGameModel gameModel)
        {
            model = gameModel;
        }

        /// <summary>
        /// Add highscores.
        /// </summary>
        /// <param name="results">results.</param>
        /// <returns>highscore renderer.</returns>
        public HighScoreRenderer AddHighScores(List<Result> results)
        {
            this.stackPanel.Width = 3 * model.GameWidth / 6;
            this.stackPanel.Height = model.GameHeight / 2;
            this.stackPanel.Orientation = Orientation.Horizontal;

            StackPanel st = new StackPanel();
            st.Height = this.stackPanel.Height;
            st.Width = this.stackPanel.Width;

            if (results != null)
            {
                int index;
                if (results.Count >= 10)
                {
                    index = 10;
                }
                else
                {
                    index = results.Count;
                }

                for (int i = 0; i < index; i++)
                {
                    StackPanel s = new StackPanel();
                    s.Orientation = Orientation.Horizontal;
                    s.Width = this.stackPanel.Width;
                    s.Height = this.stackPanel.Height / 12;

                    StackPanel s1 = new StackPanel();
                    s1.Height = this.stackPanel.Height / 12;
                    s1.Width = this.stackPanel.Width / 4;
                    s1.Orientation = Orientation.Vertical;

                    StackPanel s2 = new StackPanel();
                    s2.Height = this.stackPanel.Height / 12;
                    s2.Width = this.stackPanel.Width / 4;
                    s2.Orientation = Orientation.Vertical;

                    StackPanel s3 = new StackPanel();
                    s3.Height = this.stackPanel.Height / 12;
                    s3.Width = this.stackPanel.Width / 4;
                    s3.Orientation = Orientation.Vertical;

                    StackPanel s4 = new StackPanel();
                    s4.Height = this.stackPanel.Height / 12;
                    s4.Width = this.stackPanel.Width / 4;
                    s4.Orientation = Orientation.Vertical;

                    Label namelabel = new Label();
                    namelabel.Content = $"{i + 1}.  " + results[i].PlayerName;
                    namelabel.HorizontalAlignment = HorizontalAlignment.Left;
                    namelabel.FontSize = s4.Height / 2;
                    namelabel.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
                    Label timelabel = new Label();
                    timelabel.Content = results[i].ElapsedTime;
                    timelabel.FontSize = s4.Height / 2;
                    timelabel.HorizontalAlignment = HorizontalAlignment.Left;
                    timelabel.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
                    Label levellabel = new Label();
                    levellabel.Content = results[i].Level;
                    levellabel.HorizontalAlignment = HorizontalAlignment.Left;
                    levellabel.FontSize = s4.Height / 2;
                    levellabel.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
                    Label mode = new Label();
                    mode.Content = results[i].Mode;
                    mode.HorizontalAlignment = HorizontalAlignment.Left;
                    mode.FontSize = s4.Height / 2;
                    mode.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

                    s1.Children.Add(namelabel);
                    s2.Children.Add(timelabel);
                    s3.Children.Add(levellabel);
                    s4.Children.Add(mode);

                    s.Children.Add(s1);
                    s.Children.Add(s2);
                    s.Children.Add(s3);
                    s.Children.Add(s4);
                    st.Children.Add(s);
                }
            }

            StackPanel sp4 = new StackPanel();
            sp4.Width = st.Width;
            sp4.Height = st.Height / 6;
            sp4.HorizontalAlignment = HorizontalAlignment.Center;
            sp4.VerticalAlignment = VerticalAlignment.Bottom;
            sp4.Orientation = Orientation.Vertical;
            Button btn2 = new Button();
            btn2.Content = "BACK";
            btn2.FontSize = sp4.Height / 2;
            btn2.BorderBrush = null;
            btn2.HorizontalAlignment = HorizontalAlignment.Center;
            btn2.VerticalAlignment = VerticalAlignment.Center;
            btn2.Height = sp4.Height;
            btn2.Width = 2 * sp4.Width / 5;
            btn2.BorderThickness = new Thickness(5);
            btn2.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            btn2.Background = Brushes.Transparent;
            btn2.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            btn2.Name = "back";
            sp4.Children.Add(btn2);
            st.Children.Add(sp4);

            this.stackPanel.Children.Add(st);

            return this;
        }

        /// <summary>
        /// Get highscores.
        /// </summary>
        /// <returns>stackpanel.</returns>
        public StackPanel GetHighScores()
        {
            return this.stackPanel;
        }
    }
}
