﻿// <copyright file="NewGameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using CircuitWars.GameModel;

    /// <summary>
    /// New game renderer.
    /// </summary>
    public class NewGameRenderer
    {
        private static IGameModel model;
        private StackPanel newGameMenu = new StackPanel();

        /// <summary>
        /// Initializes a new instance of the <see cref="NewGameRenderer"/> class.
        /// </summary>
        /// <param name="gameModel">game model.</param>
        public NewGameRenderer(IGameModel gameModel)
        {
            model = gameModel;
        }

        /// <summary>
        /// Add buttons.
        /// </summary>
        /// <returns>new game renderer.</returns>
        public NewGameRenderer AddButtons()
        {
            this.newGameMenu.Width = model.GameWidth / 4;
            this.newGameMenu.Height = model.GameHeight / 2;
            this.newGameMenu.Orientation = Orientation.Vertical;

            var sp = new StackPanel();
            Label name = new Label();
            TextBox nameText = new TextBox();
            sp.Width = this.newGameMenu.Width;
            sp.Height = this.newGameMenu.Height / 5;
            sp.Orientation = Orientation.Horizontal;
            sp.HorizontalAlignment = HorizontalAlignment.Center;
            sp.VerticalAlignment = VerticalAlignment.Center;

            name.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            name.Width = sp.Width / 3;
            name.Height = sp.Height / 2;
            name.FontSize = 25;
            name.Content = "Name: ";
            name.Background = Brushes.Transparent;
            name.HorizontalAlignment = HorizontalAlignment.Center;
            name.VerticalContentAlignment = VerticalAlignment.Center;
            name.VerticalAlignment = VerticalAlignment.Center;

            nameText.Foreground = Brushes.Black;
            nameText.Width = sp.Width / 2;
            nameText.Height = sp.Height / 2;
            nameText.FontSize = sp.Height / 4;
            nameText.Text = "player01";
            nameText.VerticalContentAlignment = VerticalAlignment.Center;
            nameText.Background = Brushes.White;
            nameText.HorizontalAlignment = HorizontalAlignment.Center;
            nameText.VerticalAlignment = VerticalAlignment.Center;

            sp.Children.Add(name);
            sp.Children.Add(nameText);

            List<string> levels = new List<string>();
            levels.Add("level1");
            levels.Add("level2");
            levels.Add("level3");

            var sp3 = new StackPanel();
            var sp4 = new StackPanel();
            sp3.Orientation = Orientation.Horizontal;
            sp3.HorizontalAlignment = HorizontalAlignment.Center;
            sp3.VerticalAlignment = VerticalAlignment.Center;

            Label l = new Label();
            l.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            l.Width = sp.Width / 3;
            l.Height = sp.Height / 2;
            l.FontSize = sp.Height / 4;
            l.Content = "Level: ";
            l.Background = Brushes.Transparent;
            l.HorizontalAlignment = HorizontalAlignment.Center;
            l.VerticalContentAlignment = VerticalAlignment.Center;
            l.VerticalAlignment = VerticalAlignment.Center;

            sp3.Width = this.newGameMenu.Width;
            sp3.Height = this.newGameMenu.Height / 5;
            sp3.HorizontalAlignment = HorizontalAlignment.Center;
            sp3.VerticalAlignment = VerticalAlignment.Center;
            ComboBox comboBox = new ComboBox();
            comboBox.ItemsSource = levels;
            comboBox.Width = sp3.Width / 2;
            comboBox.Height = sp3.Height / 2;
            comboBox.FontSize = sp3.Height / 4;
            comboBox.HorizontalAlignment = HorizontalAlignment.Center;
            comboBox.VerticalAlignment = VerticalAlignment.Center;
            comboBox.Name = "level";
            sp3.Children.Add(l);
            sp3.Children.Add(comboBox);

            Label l2 = new Label();
            l2.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            l2.Width = sp.Width / 3;
            l2.Height = sp.Height / 2;
            l2.FontSize = sp3.Height / 4;
            l2.Content = "Mode: ";
            l2.Background = Brushes.Transparent;
            l2.HorizontalAlignment = HorizontalAlignment.Center;
            l2.VerticalContentAlignment = VerticalAlignment.Center;
            l2.VerticalAlignment = VerticalAlignment.Center;

            List<string> modes = new List<string>();
            modes.Add("easy");
            modes.Add("medium");
            modes.Add("hard");
            StackPanel sp5 = new StackPanel();
            sp5.Orientation = Orientation.Horizontal;

            sp5.Width = this.newGameMenu.Width;
            sp5.Height = this.newGameMenu.Height / 5;
            sp5.HorizontalAlignment = HorizontalAlignment.Center;
            sp5.VerticalAlignment = VerticalAlignment.Center;

            ComboBox c = new ComboBox();
            c.ItemsSource = modes;
            c.Width = sp5.Width / 2;
            c.Height = sp5.Height / 2;
            c.FontSize = sp5.Height / 4;
            c.HorizontalAlignment = HorizontalAlignment.Center;
            c.VerticalAlignment = VerticalAlignment.Center;
            c.Name = "mode";
            sp5.Children.Add(l2);
            sp5.Children.Add(c);

            sp4.Width = this.newGameMenu.Width;
            sp4.Height = this.newGameMenu.Height / 5;
            sp4.HorizontalAlignment = HorizontalAlignment.Center;
            sp4.VerticalAlignment = VerticalAlignment.Center;
            sp4.Orientation = Orientation.Horizontal;

            Button btn = new Button();
            btn.Content = "START";
            btn.FontSize = sp.Height / 2;
            btn.BorderBrush = null;
            btn.BorderThickness = new Thickness(5);
            btn.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            btn.Background = Brushes.Transparent;
            btn.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            btn.HorizontalAlignment = HorizontalAlignment.Center;
            btn.VerticalAlignment = VerticalAlignment.Center;
            btn.Height = sp4.Height;
            btn.Name = "start";
            btn.Width = 2 * sp4.Width / 5;

            Button btn2 = new Button();
            btn2.Content = "BACK";
            btn2.FontSize = sp.Height / 2;
            btn2.BorderBrush = null;
            btn2.Margin = new Thickness(0, 0, sp4.Width - (2 * btn.Width), 0);
            btn2.HorizontalAlignment = HorizontalAlignment.Center;
            btn2.VerticalAlignment = VerticalAlignment.Center;
            btn2.Height = sp4.Height;
            btn2.Width = 2 * sp4.Width / 5;
            btn2.BorderThickness = new Thickness(5);
            btn2.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            btn2.Background = Brushes.Transparent;
            btn2.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            btn2.Name = "back";

            sp4.Children.Add(btn2);
            sp4.Children.Add(btn);

            StackPanel s = new StackPanel();
            s.Width = this.newGameMenu.Width;
            s.Height = this.newGameMenu.Height / 7;

            this.newGameMenu.Children.Add(sp);
            this.newGameMenu.Children.Add(sp3);
            this.newGameMenu.Children.Add(sp5);
            this.newGameMenu.Children.Add(s);
            this.newGameMenu.Children.Add(sp4);

            return this;
        }

        /// <summary>
        /// Get new menu.
        /// </summary>
        /// <returns>stackpanel.</returns>
        public StackPanel GetNewGameMenu()
        {
            return this.newGameMenu;
        }
    }
}
