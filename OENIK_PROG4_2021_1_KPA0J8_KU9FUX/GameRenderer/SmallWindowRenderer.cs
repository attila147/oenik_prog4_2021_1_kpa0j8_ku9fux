﻿// <copyright file="SmallWindowRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using CircuitWars.GameModel;

    /// <summary>
    /// Small window enderer class.
    /// </summary>
    public class SmallWindowRenderer
    {
        private static IGameModel model;
        private StackPanel gameover = new StackPanel();
        private StackPanel pause = new StackPanel();
        private StackPanel won = new StackPanel();

        /// <summary>
        /// Initializes a new instance of the <see cref="SmallWindowRenderer"/> class.
        /// </summary>
        /// <param name="gameModel">game model.</param>
        public SmallWindowRenderer(IGameModel gameModel)
        {
            model = gameModel;
        }

        /// <summary>
        /// Add pause.
        /// </summary>
        /// <returns>small window renderer.</returns>
        public SmallWindowRenderer AddPause()
        {
            this.pause.Width = model.GameWidth / 5;
            this.pause.Height = model.GameHeight / 2;
            this.pause.Background = Brushes.Black;

            Label label = new Label();
            label.Content = "GAME PAUSED";
            label.FontSize = 40;
            label.HorizontalAlignment = HorizontalAlignment.Center;
            label.Height = this.pause.Height / 6;
            label.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

            Button resume = new Button();
            resume.Content = "Resume";
            resume.FontSize = 30;
            resume.HorizontalAlignment = HorizontalAlignment.Center;
            resume.Width = 6 * this.pause.Width / 7;
            resume.Height = this.pause.Height / 6;
            resume.Margin = new Thickness(0, 0, 0, this.pause.Height / 22);
            resume.BorderThickness = new Thickness(5);
            resume.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            resume.Background = Brushes.Transparent;
            resume.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            resume.Name = "resume";

            Button restart = new Button();
            restart.Content = "Restart";
            restart.FontSize = 30;
            restart.HorizontalAlignment = HorizontalAlignment.Center;
            restart.Width = 6 * this.pause.Width / 7;
            restart.Height = this.pause.Height / 6;
            restart.Margin = new Thickness(0, 0, 0, this.pause.Height / 22);
            restart.BorderThickness = new Thickness(5);
            restart.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            restart.Background = Brushes.Transparent;
            restart.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            restart.Name = "restart";

            Button save = new Button();
            save.Content = "Save game";
            save.FontSize = 30;
            save.HorizontalAlignment = HorizontalAlignment.Center;
            save.Width = 6 * this.pause.Width / 7;
            save.Height = this.pause.Height / 6;
            save.Margin = new Thickness(0, 0, 0, this.pause.Height / 22);
            save.BorderThickness = new Thickness(5);
            save.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            save.Background = Brushes.Transparent;
            save.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            save.Name = "save";

            Button back = new Button();
            back.Content = "Back to menu";
            back.FontSize = 30;
            back.HorizontalAlignment = HorizontalAlignment.Center;
            back.Width = 6 * this.pause.Width / 7;
            back.Height = this.pause.Height / 6;
            back.Margin = new Thickness(0, 0, 0, this.pause.Height / 22);
            back.BorderThickness = new Thickness(5);
            back.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            back.Background = Brushes.Transparent;
            back.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            back.Name = "back";

            this.pause.Children.Add(label);
            this.pause.Children.Add(resume);
            this.pause.Children.Add(restart);
            this.pause.Children.Add(save);
            this.pause.Children.Add(back);
            return this;
        }

        /// <summary>
        /// Add won.
        /// </summary>
        /// <param name="timeSpan">timespan.</param>
        /// <returns>small window renderer.</returns>
        public SmallWindowRenderer AddYouWon(TimeSpan timeSpan)
        {
            this.won.Width = model.GameWidth / 5;
            this.won.Height = model.GameHeight / 2;
            this.won.Background = Brushes.Black;

            Label label = new Label();
            label.Content = "YOU WON";
            label.FontSize = 40;
            label.HorizontalAlignment = HorizontalAlignment.Center;
            label.Height = this.won.Height / 6;
            label.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            string t = timeSpan.ToString(@"mm\:ss");

            Label label1 = new Label();
            label1.FontSize = 40;
            label1.HorizontalAlignment = HorizontalAlignment.Center;
            label1.Height = this.won.Height / 6;
            label1.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

            Label l1 = new Label();
            l1.Height = this.won.Height / 6;

            Button restart = new Button();
            restart.Content = "Restart";
            restart.FontSize = 30;
            restart.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            restart.Width = 6 * this.won.Width / 7;
            restart.Height = this.won.Height / 6;
            restart.BorderThickness = new Thickness(5);
            restart.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            restart.Background = Brushes.Transparent;
            restart.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            restart.Name = "restart";

            Label l = new Label();

            Button back = new Button();
            back.Content = "Back to menu";
            back.FontSize = 30;
            back.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            back.Width = 6 * this.won.Width / 7;
            back.Height = this.won.Height / 6;

            back.BorderThickness = new Thickness(5);
            back.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            back.Background = Brushes.Transparent;
            back.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            back.Name = "back";

            this.won.Children.Add(label);
            this.won.Children.Add(label1);
            this.won.Children.Add(l1);
            this.won.Children.Add(restart);
            this.won.Children.Add(l);
            this.won.Children.Add(back);
            return this;
        }

        /// <summary>
        /// Add lose.
        /// </summary>
        /// <param name="timeSpan">timespan.</param>
        /// <returns>small window renderer.</returns>
        public SmallWindowRenderer AddYouLose(TimeSpan timeSpan)
        {
            this.gameover.Width = model.GameWidth / 5;
            this.gameover.Height = model.GameHeight / 2;
            this.gameover.Background = Brushes.Black;

            Label label = new Label();
            label.Content = "YOU LOSE";
            label.FontSize = 40;
            label.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            label.Height = this.won.Height / 6;
            label.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            string t = timeSpan.ToString(@"mm\:ss");

            Label label1 = new Label();
            label1.FontSize = 40;
            label1.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            label1.Height = this.won.Height / 6;
            label1.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

            Label l1 = new Label();
            l1.Height = this.won.Height / 6;

            Button restart = new Button();
            restart.Content = "Restart";
            restart.FontSize = 30;
            restart.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            restart.Width = 6 * this.won.Width / 7;
            restart.Height = this.won.Height / 6;
            restart.BorderThickness = new Thickness(5);
            restart.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            restart.Background = Brushes.Transparent;
            restart.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            restart.Name = "restart";

            Label l = new Label();

            Button back = new Button();
            back.Content = "Back to menu";
            back.FontSize = 30;
            back.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            back.Width = 6 * this.won.Width / 7;
            back.Height = this.won.Height / 6;
            back.BorderThickness = new Thickness(5);
            back.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            back.Background = Brushes.Transparent;
            back.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            back.Name = "back";

            this.gameover.Children.Add(label);
            this.gameover.Children.Add(label1);
            this.gameover.Children.Add(l1);
            this.gameover.Children.Add(restart);
            this.gameover.Children.Add(l);
            this.gameover.Children.Add(back);
            return this;
        }

        /// <summary>
        /// Get pause.
        /// </summary>
        /// <returns>stackpanel.</returns>
        public StackPanel GetPause()
        {
            return this.pause;
        }

        /// <summary>
        /// Get won.
        /// </summary>
        /// <returns>stackpanel.</returns>
        public StackPanel GetWon()
        {
            return this.won;
        }

        /// <summary>
        /// Get lose.
        /// </summary>
        /// <returns>stackpanel.</returns>
        public StackPanel GetLose()
        {
            return this.gameover;
        }
    }
}
