﻿// <copyright file="GGameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using CircuitWars.GameModel;
    using CircuitWars.GameRepository;

    /// <summary>
    /// Game Renderer class (main).
    /// </summary>
    public class GGameRenderer : IGameRenderer
    {
        private IGameModel model;
        private Drawing oldBackground;
        private Drawing[] oldText;
        private Drawing[] oldWalls;
        private Drawing[] oldComponents;
        private List<Drawing> threads;
        private List<Drawing> electrons;
        private int[] oldThreadNum;
        private string[] oldCompOwner;
        private int[] oldTextValue;
        private Dictionary<string, Brush> brushes = new Dictionary<string, Brush>();
        private MenuRenderer menuRenderer;
        private NewGameRenderer newGameRenderer;
        private LoadGameRenderer loadGameRenderer;
        private HighScoreRenderer highScoreRenderer;
        private SmallWindowRenderer windowRenderer;

        /// <summary>
        /// Initializes a new instance of the <see cref="GGameRenderer"/> class.
        /// </summary>
        /// <param name="model">model.</param>
        /// <param name="menuRenderer">menu renderer.</param>
        /// <param name="newGameRenderer">new game renderer.</param>
        /// <param name="loadGameRenderer">load game renderer.</param>
        /// <param name="highScoreRenderer">highscore renderer.</param>
        /// <param name="smallWindowRenderer">small window renderer.</param>
        public GGameRenderer(IGameModel model, MenuRenderer menuRenderer, NewGameRenderer newGameRenderer, LoadGameRenderer loadGameRenderer, HighScoreRenderer highScoreRenderer, SmallWindowRenderer smallWindowRenderer)
        {
            this.model = model;
            this.menuRenderer = menuRenderer;
            this.newGameRenderer = newGameRenderer;
            this.loadGameRenderer = loadGameRenderer;
            this.highScoreRenderer = highScoreRenderer;
            this.windowRenderer = smallWindowRenderer;
            this.oldThreadNum = new int[this.model.Components.Length];
            this.oldCompOwner = new string[this.model.Components.Length];
            this.oldComponents = new Drawing[this.model.Components.Length];
            this.oldWalls = new Drawing[this.model.Walls.Length];
            this.oldText = new Drawing[this.model.Components.Length];
            this.oldTextValue = new int[this.oldText.Length];
        }

        private Brush BackgroundBrushMenu
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.WALLPAPER_READY.bmp", false); }
        }

        private Brush BackgroundBrushMenu3
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.DARKPCB2NEW.bmp", false); }
        }

        private Brush BackgroundBrush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.newDarkWallpaper.bmp", false); }
        }

        private Brush BackgroundBrushMenu4
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.HIGHSCORESWALLAPAPER.bmp", false); }
        }

        private Brush CPUBlue0Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUECPU0.bmp", false); }
        }

        private Brush CPUBlue1Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUECPU1.bmp", false); }
        }

        private Brush CPUBlue2Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUECPU2.bmp", false); }
        }

        private Brush GPUBlue0Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUEGPU0.bmp", false); }
        }

        private Brush GPUBlue1Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUEGPU1.bmp", false); }
        }

        private Brush GPUBlue2Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUEGPU2.bmp", false); }
        }

        private Brush GPUBlue3Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUEGPU3.bmp", false); }
        }

        private Brush ALUBlue0Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUEALU0.bmp", false); }
        }

        private Brush ALUBlue1Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUEALU1.bmp", false); }
        }

        private Brush ALUBlue2Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUEALU2.bmp", false); }
        }

        private Brush ALUBlue3Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUEALU3.bmp", false); }
        }

        private Brush ALUBlue4Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUEALU4.bmp", false); }
        }

        private Brush RAMBlue0Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUERAM0.bmp", false); }
        }

        private Brush RAMBlue1Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUERAM1.bmp", false); }
        }

        private Brush RAMBlue2Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.BLUERAM2.bmp", false); }
        }

        private Brush CPURed0Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.CPU0.bmp", false); }
        }

        private Brush CPURed1Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.CPU1.bmp", false); }
        }

        private Brush CPURed2Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.CPU2.bmp", false); }
        }

        private Brush GPURed0Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDGPU0BETER_1.bmp", false); }
        }

        private Brush GPURed1Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDGPU1.bmp", false); }
        }

        private Brush GPURed2Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDGPU2.bmp", false); }
        }

        private Brush GPURed3Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDGPU3.bmp", false); }
        }

        private Brush ALURed0Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDALU0.bmp", false); }
        }

        private Brush ALURed1Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDALU1.bmp", false); }
        }

        private Brush ALURed2Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDALU2.bmp", false); }
        }

        private Brush ALURed3Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDALU3.bmp", false); }
        }

        private Brush ALURed4Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDALU4.bmp", false); }
        }

        private Brush RAMRed0Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDRAM0.bmp", false); }
        }

        private Brush RAMRed1Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDRAM1.bmp", false); }
        }

        private Brush RAMRed2Brush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.REDRAM2.bmp", false); }
        }

        private Brush CPUGrayBrush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.CPUS.bmp", false); }
        }

        private Brush GPUGrayBrush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.comp.bmp", false); }
        }

        private Brush ALUGrayBrush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.GRAYALU0.bmp", false); }
        }

        private Brush RAMGrayBrush
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.GRAYRAM.bmp", false); }
        }

        private Brush WallBrushUp
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.WallUPGOOOD.bmp", false); }
        }

        private Brush WallBrushDown
        {
            get { return this.GetBrush("CircuitWars.GameRenderer.Images.walldownGOOOD.bmp", false); }
        }

        /// <summary>
        /// For map reset.
        /// </summary>
        public void Reset()
        {
            this.oldBackground = null;
            this.oldComponents = null;
            this.oldWalls = null;
            this.brushes.Clear();
        }

        /// <summary>
        /// calls menu renderer, for buttons.
        /// </summary>
        /// <returns>gets the bottons stack.</returns>
        public StackPanel GetNewGameMenu()
        {
            this.newGameRenderer.AddButtons();
            return this.newGameRenderer.GetNewGameMenu();
        }

        /// <summary>
        /// original.
        /// </summary>
        /// <returns>gets menu stack.</returns>
        public StackPanel GetMenu()
        {
            this.menuRenderer.AddButtons();
            return this.menuRenderer.GetMenu();
        }

        /// <summary>
        /// Get pause.
        /// </summary>
        /// <returns>stackpanel.</returns>
        public StackPanel GetPause()
        {
            this.windowRenderer.AddPause();
            return this.windowRenderer.GetPause();
        }

        /// <summary>
        /// Add won window.
        /// </summary>
        /// <param name="timeSpan">timespan.</param>
        /// <returns>stackpanel.</returns>
        public StackPanel GetWon(TimeSpan timeSpan)
        {
            this.windowRenderer.AddYouWon(timeSpan);
            return this.windowRenderer.GetWon();
        }

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>drawing.</returns>
        public Drawing BuildDrawingMenu()
        {
            DrawingGroup dg = new DrawingGroup();

            Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, this.model.GameWidth, this.model.GameHeight));
            Drawing d = new GeometryDrawing(this.BackgroundBrushMenu, null, backgroundGeometry);

            dg.Children.Add(d);

            return dg;
        }

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>drawing.</returns>
        public Drawing BuildDrawingMenu2()
        {
            DrawingGroup dg = new DrawingGroup();

            Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, this.model.GameWidth, this.model.GameHeight));
            Drawing d = new GeometryDrawing(this.BackgroundBrush, null, backgroundGeometry);

            dg.Children.Add(d);

            return dg;
        }

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>drawing.</returns>
        public Drawing BuildDrawingMenu3()
        {
            DrawingGroup dg = new DrawingGroup();

            Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, this.model.GameWidth, this.model.GameHeight));
            Drawing d = new GeometryDrawing(this.BackgroundBrushMenu3, null, backgroundGeometry);

            dg.Children.Add(d);

            return dg;
        }

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>drawing.</returns>
        public Drawing BuildDrawingMenu4()
        {
            DrawingGroup dg = new DrawingGroup();

            Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, this.model.GameWidth, this.model.GameHeight));
            Drawing d = new GeometryDrawing(this.BackgroundBrushMenu4, null, backgroundGeometry);

            dg.Children.Add(d);

            return dg;
        }

        /// <summary>
        /// Get load games.
        /// </summary>
        /// <param name="loads">loads.</param>
        /// <returns>stackpanel.</returns>
        public StackPanel GetLoadGames(List<string> loads)
        {
            this.loadGameRenderer.AddLoads(loads);
            return this.loadGameRenderer.GetLoads();
        }

        /// <summary>
        /// Get high scores.
        /// </summary>
        /// <param name="list">list.</param>
        /// <returns>stackpanel.</returns>
        public StackPanel GetHighScores(List<Result> list)
        {
            this.highScoreRenderer.AddHighScores(list);
            return this.highScoreRenderer.GetHighScores();
        }

        /// <summary>
        /// Get lose window.
        /// </summary>
        /// <param name="timeSpan">timespan.</param>
        /// <returns>stackpanel.</returns>
        public StackPanel GetLose(TimeSpan timeSpan)
        {
            this.windowRenderer.AddYouLose(timeSpan);
            return this.windowRenderer.GetLose();
        }

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>drawing.</returns>
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(this.GetBackground());
            this.threads = this.GetThreads();
            foreach (var item in this.threads)
            {
                dg.Children.Add(item);
            }

            this.electrons = this.GetElectrons();
            foreach (var item in this.electrons)
            {
                dg.Children.Add(item);
            }

            for (int j = 0; j < this.oldComponents.Length; j++)
            {
                dg.Children.Add(this.GetComponent(this.oldComponents[j], j));
            }

            for (int j = 0; j < this.oldWalls.Length; j++)
            {
                dg.Children.Add(this.GetWalls(this.oldWalls[j], j));
            }

            for (int j = 0; j < this.oldText.Length; j++)
            {
                dg.Children.Add(this.GetText(this.oldText[j], j));
            }

            return dg;
        }

        private Brush GetBrush(string fname, bool isTiled)
        {
            if (!this.brushes.ContainsKey(fname))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);

                if (isTiled)
                {
                    ib.TileMode = TileMode.Tile;
                    ib.Viewport = new Rect(0, 0, this.model.TileSize, this.model.TileSize);
                    ib.ViewportUnits = BrushMappingMode.Absolute;
                }

                this.brushes.Add(fname, ib);
            }

            return this.brushes[fname];
        }

        private List<Drawing> GetThreads()
        {
            List<Drawing> list = new List<Drawing>();
            foreach (var item in this.model.Threads.ToList())
            {
                if (item.Owner == "player" && !item.Battle)
                {
                    LineGeometry lg = new LineGeometry(item.StartingPoint, item.EndPoint);
                    GeometryDrawing gd = new GeometryDrawing(null, new Pen(Brushes.Blue, 5), lg);
                    list.Add(gd);
                }
                else if (item.Owner == "AI" && !item.Battle)
                {
                    LineGeometry lg = new LineGeometry(item.StartingPoint, item.EndPoint);
                    GeometryDrawing gd = new GeometryDrawing(null, new Pen(Brushes.Red, 5), lg);
                    list.Add(gd);
                }
                else
                {
                    if (item.Owner == "player")
                    {
                        Point point = new Point(item.StartingPoint.X + ((item.EndPoint.X - item.StartingPoint.X) / 2), item.StartingPoint.Y + ((item.EndPoint.Y - item.StartingPoint.Y) / 2));
                        LineGeometry lg = new LineGeometry(item.StartingPoint, point);
                        GeometryDrawing gd = new GeometryDrawing(null, new Pen(Brushes.Blue, 5), lg);
                        list.Add(gd);
                    }
                    else
                    {
                        Point point = new Point(item.StartingPoint.X + ((item.EndPoint.X - item.StartingPoint.X) / 2), item.StartingPoint.Y + ((item.EndPoint.Y - item.StartingPoint.Y) / 2));
                        LineGeometry lg = new LineGeometry(item.StartingPoint, point);
                        GeometryDrawing gd = new GeometryDrawing(null, new Pen(Brushes.Red, 5), lg);
                        list.Add(gd);
                    }
                }
            }

            return list;
        }

        private List<Drawing> GetElectrons()
        {
            List<Drawing> list = new List<Drawing>();
            foreach (var item in this.model.Threads.ToList())
            {
                foreach (var i in item.Electrons)
                {
                    if (i.Owner == "player")
                    {
                        EllipseGeometry eg = new EllipseGeometry(i.Point, (i.Charge * this.model.TileSize / 20) > 10 ? 13 : 7, (i.Charge * this.model.TileSize / 20) > 10 ? 13 : 7);
                        GeometryDrawing gd = new GeometryDrawing(Brushes.Yellow, new Pen(Brushes.Blue, eg.RadiusX / 2), eg);
                        list.Add(gd);
                    }
                    else if (i.Owner == "AI")
                    {
                        EllipseGeometry eg = new EllipseGeometry(i.Point, (i.Charge * this.model.TileSize / 20) > 10 ? 13 : 7, (i.Charge * this.model.TileSize / 20) > 10 ? 13 : 7);
                        GeometryDrawing gd = new GeometryDrawing(Brushes.Yellow, new Pen(Brushes.Red, eg.RadiusX / 2), eg);
                        list.Add(gd);
                    }
                    else
                    {
                        EllipseGeometry eg = new EllipseGeometry(i.Point, (i.Charge * this.model.TileSize / 20) > 10 ? 13 : 7, (i.Charge * this.model.TileSize / 20) > 10 ? 13 : 7);
                        GeometryDrawing gd = new GeometryDrawing(Brushes.Yellow, new Pen(Brushes.Yellow, eg.RadiusX / 2), eg);
                        list.Add(gd);
                    }
                }
            }

            return list;
        }

        private Drawing GetBackground()
        {
            if (this.oldBackground == null)
            {
                Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, this.model.GameWidth, this.model.GameHeight));
                this.oldBackground = new GeometryDrawing(this.BackgroundBrush, null, backgroundGeometry);
            }

            return this.oldBackground;
        }

        private Drawing GetComponent(Drawing old, int i)
        {
            if (this.model.Components[i].ActThread != this.oldThreadNum[i] || this.model.Components[i].Owner != this.oldCompOwner[i] || old == null)
            {
                if (this.model.Components[i].Owner == "player")
                {
                    if (this.model.Components[i].Name == "CPU")
                    {
                        if (this.model.Components[i].ActThread == 0)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.CPUBlue0Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 0;
                            this.oldCompOwner[i] = "player";
                        }
                        else if (this.model.Components[i].ActThread == 1)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.CPUBlue1Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 1;
                            this.oldCompOwner[i] = "player";
                        }
                        else
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.CPUBlue2Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 2;
                            this.oldCompOwner[i] = "player";
                        }
                    }
                    else if (this.model.Components[i].Name == "GPU")
                    {
                        if (this.model.Components[i].ActThread == 0)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.GPUBlue0Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 0;
                            this.oldCompOwner[i] = "player";
                        }
                        else if (this.model.Components[i].ActThread == 1)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.GPUBlue1Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 1;
                            this.oldCompOwner[i] = "player";
                        }
                        else if (this.model.Components[i].ActThread == 2)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.GPUBlue2Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 2;
                            this.oldCompOwner[i] = "player";
                        }
                        else
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.GPUBlue3Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 3;
                            this.oldCompOwner[i] = "player";
                        }
                    }
                    else if (this.model.Components[i].Name == "ALU")
                    {
                        if (this.model.Components[i].ActThread == 0)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.ALUBlue0Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 0;
                            this.oldCompOwner[i] = "player";
                        }
                        else if (this.model.Components[i].ActThread == 1)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.ALUBlue1Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 1;
                            this.oldCompOwner[i] = "player";
                        }
                        else if (this.model.Components[i].ActThread == 2)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.ALUBlue2Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 2;
                            this.oldCompOwner[i] = "player";
                        }
                        else if (this.model.Components[i].ActThread == 3)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.ALUBlue3Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 3;
                            this.oldCompOwner[i] = "player";
                        }
                        else
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.ALUBlue4Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 4;
                            this.oldCompOwner[i] = "player";
                        }
                    }
                    else
                    {
                        if (this.model.Components[i].ActThread == 0)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.RAMBlue0Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 0;
                            this.oldCompOwner[i] = "player";
                        }
                        else if (this.model.Components[i].ActThread == 1)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.RAMBlue1Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 1;
                            this.oldCompOwner[i] = "player";
                        }
                        else
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.RAMBlue2Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 2;
                            this.oldCompOwner[i] = "player";
                        }
                    }
                }
                else if (this.model.Components[i].Owner == "AI")
                {
                    if (this.model.Components[i].Name == "CPU")
                    {
                        if (this.model.Components[i].ActThread == 0)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.CPURed0Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 0;
                            this.oldCompOwner[i] = "AI";
                        }
                        else if (this.model.Components[i].ActThread == 1)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.CPURed1Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 1;
                            this.oldCompOwner[i] = "AI";
                        }
                        else
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.CPURed2Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 2;
                            this.oldCompOwner[i] = "AI";
                        }
                    }
                    else if (this.model.Components[i].Name == "GPU")
                    {
                        if (this.model.Components[i].ActThread == 0)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.GPURed0Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 0;
                            this.oldCompOwner[i] = "AI";
                        }
                        else if (this.model.Components[i].ActThread == 1)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.GPURed1Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 1;
                            this.oldCompOwner[i] = "AI";
                        }
                        else if (this.model.Components[i].ActThread == 2)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.GPURed2Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 2;
                            this.oldCompOwner[i] = "AI";
                        }
                        else
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.GPURed3Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 3;
                            this.oldCompOwner[i] = "AI";
                        }
                    }
                    else if (this.model.Components[i].Name == "ALU")
                    {
                        if (this.model.Components[i].ActThread == 0)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.ALURed0Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 0;
                            this.oldCompOwner[i] = "AI";
                        }
                        else if (this.model.Components[i].ActThread == 1)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.ALURed1Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 1;
                            this.oldCompOwner[i] = "AI";
                        }
                        else if (this.model.Components[i].ActThread == 2)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.ALURed2Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 2;
                            this.oldCompOwner[i] = "AI";
                        }
                        else if (this.model.Components[i].ActThread == 3)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.ALURed3Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 3;
                            this.oldCompOwner[i] = "AI";
                        }
                        else
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.ALURed4Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 4;
                            this.oldCompOwner[i] = "AI";
                        }
                    }
                    else
                    {
                        if (this.model.Components[i].ActThread == 0)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.RAMRed0Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 0;
                            this.oldCompOwner[i] = "AI";
                        }
                        else if (this.model.Components[i].ActThread == 1)
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.RAMRed1Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 1;
                            this.oldCompOwner[i] = "AI";
                        }
                        else
                        {
                            Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            old = new GeometryDrawing(this.RAMRed2Brush, null, componentGeometry);
                            this.oldThreadNum[i] = 2;
                            this.oldCompOwner[i] = "AI";
                        }
                    }
                }
                else
                {
                    if (this.model.Components[i].Name == "CPU")
                    {
                        Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                        old = new GeometryDrawing(this.CPUGrayBrush, null, componentGeometry);
                        this.oldThreadNum[i] = 0;
                        this.oldCompOwner[i] = "Neutral";
                    }
                    else if (this.model.Components[i].Name == "GPU")
                    {
                        Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                        old = new GeometryDrawing(this.GPUGrayBrush, null, componentGeometry);
                        this.oldThreadNum[i] = 0;
                        this.oldCompOwner[i] = "Neutral";
                    }
                    else if (this.model.Components[i].Name == "ALU")
                    {
                        Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                        old = new GeometryDrawing(this.ALUGrayBrush, null, componentGeometry);
                        this.oldThreadNum[i] = 0;
                        this.oldCompOwner[i] = "Neutral";
                    }
                    else
                    {
                        Geometry componentGeometry = new RectangleGeometry(new Rect(this.model.Components[i].Point.X * this.model.TileSize, this.model.Components[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                        old = new GeometryDrawing(this.RAMGrayBrush, null, componentGeometry);
                        this.oldThreadNum[i] = 0;
                        this.oldCompOwner[i] = "Neutral";
                    }
                }

                this.oldComponents[i] = null;
                this.oldComponents[i] = old;
            }

            return old;
        }

        private Drawing GetWalls(Drawing old, int i)
        {
            if (this.oldWalls[i] == null)
            {
                if (this.model.Walls[i].Name == "up")
                {
                    Geometry box = new RectangleGeometry(new Rect(this.model.Walls[i].Point.X * this.model.TileSize, this.model.Walls[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                    old = new GeometryDrawing(this.WallBrushUp, null, box);
                    this.oldWalls[i] = old;
                }
                else
                {
                    Geometry box = new RectangleGeometry(new Rect(this.model.Walls[i].Point.X * this.model.TileSize, this.model.Walls[i].Point.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                    old = new GeometryDrawing(this.WallBrushDown, null, box);
                    this.oldWalls[i] = old;
                }
            }

            return old;
        }

        private Drawing GetText(Drawing old, int i)
        {
            if (old == null || this.oldTextValue[i] != this.model.Components[i].ChargeLevel)
            {
                if (this.model.Components[i].Name == "CPU")
                {
                    if (this.model.Components[i].ChargeLevel >= 10 && this.model.Components[i].ChargeLevel < 100)
                    {
                        FormattedText ft = new FormattedText(
                        this.model.Components[i].ChargeLevel.ToString(),
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        this.model.TileSize / 5,
                        Brushes.Black,
                        1);
                        old = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), ft.BuildGeometry(new Point((this.model.Components[i].Point.X * this.model.TileSize) + (this.model.TileSize * 0.4), (this.model.Components[i].Point.Y * this.model.TileSize) + (this.model.TileSize * 0.4))));
                    }
                    else if (this.model.Components[i].ChargeLevel < 10)
                    {
                        FormattedText ft = new FormattedText(
                        this.model.Components[i].ChargeLevel.ToString(),
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        this.model.TileSize / 5,
                        Brushes.Black,
                        1);
                        old = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), ft.BuildGeometry(new Point((this.model.Components[i].Point.X * this.model.TileSize) + (this.model.TileSize * 0.45), (this.model.Components[i].Point.Y * this.model.TileSize) + (this.model.TileSize * 0.4))));
                    }
                    else
                    {
                        FormattedText ft = new FormattedText(
                        this.model.Components[i].ChargeLevel.ToString(),
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        this.model.TileSize / 5,
                        Brushes.Black,
                        1);
                        old = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), ft.BuildGeometry(new Point((this.model.Components[i].Point.X * this.model.TileSize) + (this.model.TileSize * 0.33), (this.model.Components[i].Point.Y * this.model.TileSize) + (this.model.TileSize * 0.4))));
                    }
                }
                else if (this.model.Components[i].Name == "GPU")
                {
                    if (this.model.Components[i].ChargeLevel >= 10 && this.model.Components[i].ChargeLevel < 100)
                    {
                        FormattedText ft = new FormattedText(
                        this.model.Components[i].ChargeLevel.ToString(),
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        this.model.TileSize / 3,
                        Brushes.Black,
                        1);
                        old = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), ft.BuildGeometry(new Point((this.model.Components[i].Point.X * this.model.TileSize) + (this.model.TileSize * 0.3), (this.model.Components[i].Point.Y * this.model.TileSize) + (this.model.TileSize * 0.3))));
                    }
                    else if (this.model.Components[i].ChargeLevel < 10)
                    {
                        FormattedText ft = new FormattedText(
                        this.model.Components[i].ChargeLevel.ToString(),
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        this.model.TileSize / 3,
                        Brushes.Black,
                        1);
                        old = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), ft.BuildGeometry(new Point((this.model.Components[i].Point.X * this.model.TileSize) + (this.model.TileSize * 0.40), (this.model.Components[i].Point.Y * this.model.TileSize) + (this.model.TileSize * 0.3))));
                    }
                }
                else if (this.model.Components[i].Name == "ALU")
                {
                    if (this.model.Components[i].ChargeLevel >= 10 && this.model.Components[i].ChargeLevel < 100)
                    {
                        FormattedText ft = new FormattedText(
                        this.model.Components[i].ChargeLevel.ToString(),
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        this.model.TileSize / 4.5,
                        Brushes.Black,
                        1);
                        old = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), ft.BuildGeometry(new Point((this.model.Components[i].Point.X * this.model.TileSize) + (this.model.TileSize * 0.3), (this.model.Components[i].Point.Y * this.model.TileSize) + (this.model.TileSize * 0.25))));
                    }
                    else if (this.model.Components[i].ChargeLevel < 10)
                    {
                        FormattedText ft = new FormattedText(
                        this.model.Components[i].ChargeLevel.ToString(),
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        this.model.TileSize / 4,
                        Brushes.Black,
                        1);
                        old = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), ft.BuildGeometry(new Point((this.model.Components[i].Point.X * this.model.TileSize) + (this.model.TileSize * 0.3), (this.model.Components[i].Point.Y * this.model.TileSize) + (this.model.TileSize * 0.25))));
                    }
                }
                else
                {
                    if (this.model.Components[i].ChargeLevel >= 10 && this.model.Components[i].ChargeLevel < 100)
                    {
                        FormattedText ft = new FormattedText(
                        this.model.Components[i].ChargeLevel.ToString(),
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        this.model.TileSize / 4,
                        Brushes.Black,
                        1);
                        old = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), ft.BuildGeometry(new Point((this.model.Components[i].Point.X * this.model.TileSize) + (this.model.TileSize * 0.3), (this.model.Components[i].Point.Y * this.model.TileSize) + (this.model.TileSize * 0.3))));
                    }
                    else if (this.model.Components[i].ChargeLevel < 10)
                    {
                        FormattedText ft = new FormattedText(
                        this.model.Components[i].ChargeLevel.ToString(),
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        this.model.TileSize / 4,
                        Brushes.Black,
                        1);
                        old = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), ft.BuildGeometry(new Point((this.model.Components[i].Point.X * this.model.TileSize) + (this.model.TileSize * 0.4), (this.model.Components[i].Point.Y * this.model.TileSize) + (this.model.TileSize * 0.3))));
                    }
                    else
                    {
                        FormattedText ft = new FormattedText(
                        this.model.Components[i].ChargeLevel.ToString(),
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Arial"),
                        this.model.TileSize / 4,
                        Brushes.Black,
                        1);
                        old = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), ft.BuildGeometry(new Point((this.model.Components[i].Point.X * this.model.TileSize) + (this.model.TileSize * 0.22), (this.model.Components[i].Point.Y * this.model.TileSize) + (this.model.TileSize * 0.3))));
                    }
                }

                this.oldTextValue[i] = this.model.Components[i].ChargeLevel;
                this.oldText[i] = old;
            }

            return old;
        }
    }
}
