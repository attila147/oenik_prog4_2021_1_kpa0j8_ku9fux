﻿// <copyright file="LoadGameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using CircuitWars.GameModel;

    /// <summary>
    /// Load game renderer class.
    /// </summary>
    public class LoadGameRenderer
    {
        private static IGameModel model;
        private StackPanel stackPanel = new StackPanel();

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadGameRenderer"/> class.
        /// </summary>
        /// <param name="gameModel">game model.</param>
        public LoadGameRenderer(IGameModel gameModel)
        {
            model = gameModel;
        }

        /// <summary>
        /// Add loads.
        /// </summary>
        /// <param name="loads">loads.</param>
        /// <returns>load game renderer.</returns>
        public LoadGameRenderer AddLoads(List<string> loads)
        {
            this.stackPanel.Width = model.GameWidth / 4;
            this.stackPanel.Height = model.GameHeight / 2;
            this.stackPanel.Orientation = Orientation.Vertical;

            var sp3 = new StackPanel();
            var sp4 = new StackPanel();
            sp3.Orientation = Orientation.Horizontal;
            sp3.Width = this.stackPanel.Width;
            sp3.Height = this.stackPanel.Height / 3;
            sp3.HorizontalAlignment = HorizontalAlignment.Center;
            sp3.VerticalAlignment = VerticalAlignment.Center;
            ComboBox comboBox = new ComboBox();
            comboBox.ItemsSource = loads;
            comboBox.Width = sp3.Width / 2;
            comboBox.Height = sp3.Height / 2;
            comboBox.FontSize = sp3.Height / 4;
            comboBox.HorizontalAlignment = HorizontalAlignment.Center;
            comboBox.VerticalAlignment = VerticalAlignment.Center;
            comboBox.Name = "level";

            Label l = new Label();
            l.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            l.Width = sp3.Width / 3;
            l.Height = sp3.Height / 2;
            l.FontSize = sp3.Height / 4;
            l.Content = "Level: ";
            l.Background = Brushes.Transparent;
            l.HorizontalAlignment = HorizontalAlignment.Center;
            l.VerticalContentAlignment = VerticalAlignment.Center;
            l.VerticalAlignment = VerticalAlignment.Center;

            sp3.Children.Add(l);
            sp3.Children.Add(comboBox);

            sp4.Width = this.stackPanel.Width;
            sp4.Height = this.stackPanel.Height / 5;
            sp4.HorizontalAlignment = HorizontalAlignment.Center;
            sp4.VerticalAlignment = VerticalAlignment.Center;
            sp4.Orientation = Orientation.Horizontal;

            Button btn = new Button();
            btn.Content = "START";
            btn.FontSize = sp4.Height / 2;
            btn.BorderBrush = null;
            btn.BorderThickness = new Thickness(5);
            btn.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            btn.Background = Brushes.Transparent;
            btn.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            btn.HorizontalAlignment = HorizontalAlignment.Center;
            btn.VerticalAlignment = VerticalAlignment.Center;
            btn.Name = "start";
            btn.Height = sp4.Height;
            btn.Width = 2 * sp4.Width / 5;

            Button btn2 = new Button();
            btn2.Content = "BACK";
            btn2.FontSize = sp4.Height / 2;
            btn2.BorderBrush = null;
            btn2.Margin = new Thickness(0, 0, sp4.Width - (2 * btn.Width), 0);
            btn2.HorizontalAlignment = HorizontalAlignment.Center;
            btn2.VerticalAlignment = VerticalAlignment.Center;
            btn2.Height = sp4.Height;
            btn2.Width = 2 * sp4.Width / 5;
            btn2.BorderThickness = new Thickness(5);
            btn2.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            btn2.Background = Brushes.Transparent;
            btn2.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            btn2.Name = "back";

            sp4.Children.Add(btn2);
            sp4.Children.Add(btn);

            StackPanel s = new StackPanel();
            s.Width = this.stackPanel.Width;
            s.Height = this.stackPanel.Height / 3;

            this.stackPanel.Children.Add(sp3);
            this.stackPanel.Children.Add(s);
            this.stackPanel.Children.Add(sp4);
            return this;
        }

        /// <summary>
        /// Get loads.
        /// </summary>
        /// <returns>stackpanel.</returns>
        internal StackPanel GetLoads()
        {
            return this.stackPanel;
        }
    }
}
