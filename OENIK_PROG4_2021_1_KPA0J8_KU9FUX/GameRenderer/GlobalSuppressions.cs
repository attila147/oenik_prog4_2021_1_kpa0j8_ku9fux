﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.LoadGameRenderer.AddLoads(System.Collections.Generic.List{System.String})~CircuitWars.GameRenderer.LoadGameRenderer")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.GGameRenderer.GetText(System.Windows.Media.Drawing,System.Int32)~System.Windows.Media.Drawing")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "Uninteresting")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.IGameRenderer.GetHighScores(System.Collections.Generic.List{CircuitWars.GameRepository.Result})~System.Windows.Controls.StackPanel")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "Uninteresting")]
[assembly: SuppressMessage("Design", "CA1024:Use properties where appropriate", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.SmallWindowRenderer.GetPause~System.Windows.Controls.StackPanel")]
[assembly: SuppressMessage("Design", "CA1024:Use properties where appropriate", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.SmallWindowRenderer.GetWon~System.Windows.Controls.StackPanel")]
[assembly: SuppressMessage("Design", "CA1024:Use properties where appropriate", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.SmallWindowRenderer.GetLose~System.Windows.Controls.StackPanel")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.SmallWindowRenderer.AddYouLose(System.TimeSpan)~CircuitWars.GameRenderer.SmallWindowRenderer")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.SmallWindowRenderer.AddYouWon(System.TimeSpan)~CircuitWars.GameRenderer.SmallWindowRenderer")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.HighScoreRenderer.AddHighScores(System.Collections.Generic.List{CircuitWars.GameRepository.Result})~CircuitWars.GameRenderer.HighScoreRenderer")]
[assembly: SuppressMessage("Design", "CA1024:Use properties where appropriate", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.HighScoreRenderer.GetHighScores~System.Windows.Controls.StackPanel")]
[assembly: SuppressMessage("Design", "CA1024:Use properties where appropriate", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.MenuRenderer.GetMenu~System.Windows.Controls.StackPanel")]
[assembly: SuppressMessage("Design", "CA1024:Use properties where appropriate", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.NewGameRenderer.GetNewGameMenu~System.Windows.Controls.StackPanel")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRenderer.IGameRenderer.GetLoadGames(System.Collections.Generic.List{System.String})~System.Windows.Controls.StackPanel")]
