﻿// <copyright file="IGameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Controls;
    using System.Windows.Media;
    using CircuitWars.GameRepository;

    /// <summary>
    /// Game renderer interface.
    /// </summary>
    public interface IGameRenderer
    {
        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>drawing.</returns>
        Drawing BuildDrawing();

        /// <summary>
        /// Reset.
        /// </summary>
        void Reset();

        /// <summary>
        /// original.
        /// </summary>
        /// <returns>gets menu stack.</returns>
        StackPanel GetMenu();

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>drawing.</returns>
        Drawing BuildDrawingMenu();

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>drawing.</returns>
        Drawing BuildDrawingMenu2();

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>drawing.</returns>
        Drawing BuildDrawingMenu3();

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>drawing.</returns>
        Drawing BuildDrawingMenu4();

        /// <summary>
        /// Get load games.
        /// </summary>
        /// <param name="loads">loads.</param>
        /// <returns>stackpanel.</returns>
        StackPanel GetLoadGames(List<string> loads);

        /// <summary>
        /// Get high scores.
        /// </summary>
        /// <param name="list">list.</param>
        /// <returns>stackpanel.</returns>
        StackPanel GetHighScores(List<Result> list);

        /// <summary>
        /// Get pause.
        /// </summary>
        /// <returns>stackpanel.</returns>
        StackPanel GetPause();

        /// <summary>
        /// Add won window.
        /// </summary>
        /// <param name="timeSpan">timespan.</param>
        /// <returns>stackpanel.</returns>
        StackPanel GetWon(TimeSpan timeSpan);

        /// <summary>
        /// Get lose window.
        /// </summary>
        /// <param name="timeSpan">timespan.</param>
        /// <returns>stackpanel.</returns>
        StackPanel GetLose(TimeSpan timeSpan);
    }
}
