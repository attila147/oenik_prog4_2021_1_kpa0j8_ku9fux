﻿// <copyright file="MenuRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using CircuitWars.GameModel;

    /// <summary>
    /// Menu renderer class.
    /// </summary>
    public class MenuRenderer
    {
        private static IGameModel model;
        private StackPanel menu = new StackPanel();

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuRenderer"/> class.
        /// </summary>
        /// <param name="gameModel">game model.</param>
        public MenuRenderer(IGameModel gameModel)
        {
            model = gameModel;
        }

        /// <summary>
        /// Add buttons.
        /// </summary>
        /// <returns>menu renderer.</returns>
        public MenuRenderer AddButtons()
        {
            this.menu.Width = model.GameWidth / 5;
            this.menu.Height = model.GameHeight / 5 * 3;

            Button menuButton = new Button();
            menuButton.Foreground = Brushes.Black;
            menuButton.BorderBrush = null;
            menuButton.Width = this.menu.Width;
            menuButton.Height = this.menu.Height / 5;
            menuButton.Content = "New game";
            menuButton.Background = Brushes.LightCyan;
            menuButton.FontSize = 50;
            menuButton.Margin = new Thickness(0, this.menu.Height / 19, 0, this.menu.Height / 20);
            menuButton.BorderThickness = new Thickness(5);
            menuButton.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            menuButton.Background = Brushes.Transparent;
            menuButton.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

            Button menuButton2 = new Button();
            menuButton2.Width = this.menu.Width;
            menuButton2.Height = this.menu.Height / 5;
            menuButton2.FontSize = 50;
            menuButton2.Content = "Load game";
            menuButton2.Background = Brushes.Transparent;
            menuButton2.Margin = new Thickness(0, 0, 0, this.menu.Height / 20);
            menuButton2.BorderThickness = new Thickness(5);
            menuButton2.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            menuButton2.Background = Brushes.Transparent;
            menuButton2.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

            Button b4 = new Button();

            b4.Foreground = Brushes.Black;
            b4.BorderBrush = null;
            b4.Width = this.menu.Width;
            b4.Height = this.menu.Height / 5;
            b4.FontSize = 50;
            b4.Content = "Highscore";
            b4.Background = Brushes.Transparent;
            b4.Background = Brushes.LightCyan;
            b4.BorderThickness = new Thickness(5);
            b4.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            b4.Background = Brushes.Transparent;
            b4.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            b4.Margin = new Thickness(0, 0, 0, this.menu.Height / 20);

            Button menuButton3 = new Button();

            menuButton3.Foreground = Brushes.Black;
            menuButton3.BorderBrush = null;
            menuButton3.Width = this.menu.Width;
            menuButton3.Height = this.menu.Height / 5;
            menuButton3.Content = "Exit";
            menuButton3.FontSize = 50;
            menuButton3.Background = Brushes.Transparent;
            menuButton3.HorizontalAlignment = HorizontalAlignment.Center;
            menuButton3.VerticalAlignment = VerticalAlignment.Center;
            menuButton3.Margin = new Thickness(0, 0, 0, this.menu.Height / 20);
            menuButton3.BorderThickness = new Thickness(5);
            menuButton3.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            menuButton3.Background = Brushes.Transparent;
            menuButton3.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));

            this.menu.Children.Add(menuButton);
            this.menu.Children.Add(menuButton2);
            this.menu.Children.Add(b4);
            this.menu.Children.Add(menuButton3);

            return this;
        }

        /// <summary>
        /// Get menu.
        /// </summary>
        /// <returns>stackpanel.</returns>
        public StackPanel GetMenu()
        {
            return this.menu;
        }
    }
}
