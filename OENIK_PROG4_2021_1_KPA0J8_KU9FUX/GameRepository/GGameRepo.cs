﻿// <copyright file="GGameRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameRepository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <summary>
    /// Game repository class.
    /// </summary>
    public class GGameRepo : IGameRepository
    {
        private static string loadfname = "CircuitWars.GameRepository.Save.load.xml";

        /// <summary>
        /// Add load xml.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">name.</param>
        public static void AddLoadXml(int id, string name)
        {
            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream(loadfname);
            XDocument xdoc = XDocument.Load(s);
            XElement xElement = new XElement("load");
            xElement.Add(new XAttribute("id", id));
            XElement e = new XElement("fname");
            e.Add($"{id}_{name}");
            xElement.Add(e);
            xdoc.Root.Add(xElement);
            string fname = string.Empty;
            string[] path = Directory.GetCurrentDirectory().Split('\\');
            for (int i = 0; i < path.Length - 4; i++)
            {
                fname += path[i] + "\\";
            }

            fname = fname + $"GameRepository\\Save\\load.xml";
            xdoc.Save(fname);
        }

        /// <summary>
        /// Loads.
        /// </summary>
        /// <returns>list.</returns>
        public static List<string> Loads()
        {
            Stream lstream = Assembly.GetExecutingAssembly().GetManifestResourceStream(loadfname);
            XDocument xdoc = XDocument.Load(lstream);
            Dictionary<int, string> d = new Dictionary<int, string>();
            var loads = from x in xdoc.Descendants("loads")
                        select new
                        {
                            id = x.Element("load").Attribute("id").Value,
                            fname = x.Element("load").Element("fname").Value,
                        };
            foreach (var item in loads)
            {
                d.Add(int.Parse(item.id), item.fname);
            }

            return null;
        }

        /// <summary>
        /// Get load games.
        /// </summary>
        /// <returns>list.</returns>
        public List<string> GetLoadGames()
        {
            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream(loadfname);
            XDocument xdoc = XDocument.Load(s);
            var loads = from x in xdoc.Descendants("load")
                        select new
                        {
                            fn = x.Element("fname").Value,
                        };
            List<string> list = new List<string>();
            foreach (var item in loads)
            {
                list.Add(item.fn);
            }

            return list;
        }

        /// <summary>
        /// Get load number.
        /// </summary>
        /// <returns>int.</returns>
        public int GetLoadNumber()
        {
            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream(loadfname);
            XDocument xdoc = XDocument.Load(s);
            var loads = (from x in xdoc.Descendants("load")
                         select new
                         {
                             id = x.Attribute("id").Value,
                         }).ToList();
            if (loads.Count != 0)
            {
                return int.Parse(loads.Last().id) + 1;
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Save game.
        /// </summary>
        /// <param name="xdoc">xdoc.</param>
        /// <param name="name">name.</param>
        public void SaveGame(ref XDocument xdoc, string name)
        {
            string fname = string.Empty;
            int id = this.GetLoadNumber();
            string[] path = Directory.GetCurrentDirectory().Split('\\');

            for (int i = 0; i < path.Length - 4; i++)
            {
                fname += path[i] + "\\";
            }

            fname += $"GameRepository\\Map\\{id}_{name}.xml";
            if (xdoc != null && !string.IsNullOrEmpty(loadfname))
            {
                xdoc.Save(fname);
            }

            AddLoadXml(id, name);
        }

        /// <summary>
        /// Save high scores.
        /// </summary>
        /// <param name="playername">player name.</param>
        /// <param name="timeSpan">timespan.</param>
        /// <param name="level">level.</param>
        /// <param name="mode">mode.</param>
        public void SaveHighscores(string playername, TimeSpan timeSpan, string level, int mode)
        {
            XElement result = new XElement("result");
            result.Add(new XAttribute("name", playername));
            result.Add(new XAttribute("time", timeSpan.ToString(@"mm\:ss")));
            result.Add(new XAttribute("level", level));
            string modestring = string.Empty;
            if (mode == 3488)
            {
                modestring = "easy";
            }
            else if (mode == 2608)
            {
                modestring = "medium";
            }
            else if (mode == 1392)
            {
                modestring = "hard";
            }

            result.Add(new XAttribute("mode", modestring));

            string fname = string.Empty;
            string[] path = Directory.GetCurrentDirectory().Split('\\');
            for (int i = 0; i < path.Length - 4; i++)
            {
                fname += path[i] + "\\";
            }

            fname = fname + $"GameRepository\\Save\\results.xml";

            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
            XDocument xdoc = XDocument.Load(fname);

            xdoc.Root.Add(result);

            xdoc.Save(fname);
        }

        /// <summary>
        /// Get high scores.
        /// </summary>
        /// <returns>list.</returns>
        public List<Result> GetHighScores()
        {
            string fname = string.Empty;
            string[] path = Directory.GetCurrentDirectory().Split('\\');
            for (int i = 0; i < path.Length - 4; i++)
            {
                fname += path[i] + "\\";
            }

            fname = fname + $"GameRepository\\Save\\results.xml";
            XDocument xdoc = XDocument.Load(fname);
            var highscores = from x in xdoc.Descendants("result")
                             select new
                             {
                                 name = x.Attribute("name").Value,
                                 time = TimeSpan.Parse(x.Attribute("time").Value),
                                 level = x.Attribute("level").Value,
                                 mode = x.Attribute("mode").Value,
                             };
            List<Result> list = new List<Result>();
            foreach (var item in highscores)
            {
                list.Add(new Result(item.name, item.time, item.level, item.mode));
            }

            List<Result> orderedlist = list.OrderBy(x => x.ElapsedTime).ToList();
            return orderedlist;
        }
    }
}
