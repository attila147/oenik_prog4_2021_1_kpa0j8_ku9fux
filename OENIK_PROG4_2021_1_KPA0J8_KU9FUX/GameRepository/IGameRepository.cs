﻿// <copyright file="IGameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <summary>
    /// Repository interface.
    /// </summary>
    public interface IGameRepository
    {
        /// <summary>
        /// Gets load games names in a list.
        /// </summary>
        /// <returns>list of names.</returns>
        List<string> GetLoadGames();

        /// <summary>
        /// Gets the number of load games.
        /// </summary>
        /// <returns>how many.</returns>
        int GetLoadNumber();

        /// <summary>
        /// Allowes to save a game.
        /// </summary>
        /// <param name="xdoc">xml.</param>
        /// <param name="name">name.</param>
        void SaveGame(ref XDocument xdoc, string name);

        /// <summary>
        /// For saving high socres.
        /// </summary>
        /// <param name="playername">players name.</param>
        /// <param name="timeSpan">time spent.</param>
        /// <param name="level">levels name.</param>
        /// <param name="mode">easy hard or medium.</param>
        public void SaveHighscores(string playername, TimeSpan timeSpan, string level, int mode);

        /// <summary>
        /// Gets high scores.
        /// </summary>
        /// <returns>a list of results.</returns>
        List<Result> GetHighScores();
    }
}
