﻿// <copyright file="Result.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameRepository
{
    using System;

    /// <summary>
    /// Result class.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Result"/> class.
        /// </summary>
        /// <param name="playerName">name.</param>
        /// <param name="elapsedTime">time.</param>
        /// <param name="level">level.</param>
        /// <param name="mode">mode.</param>
        public Result(string playerName, TimeSpan elapsedTime, string level, string mode)
        {
            this.PlayerName = playerName;
            this.ElapsedTime = elapsedTime;
            this.Level = level;
            this.Mode = mode;
        }

        /// <summary>
        /// Gets or sets player name.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets elapsed time..
        /// </summary>
        public TimeSpan ElapsedTime { get; set; }

        /// <summary>
        /// Gets or sets playername.
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// Gets or sets mode.
        /// </summary>
        public string Mode { get; set; }
    }
}
