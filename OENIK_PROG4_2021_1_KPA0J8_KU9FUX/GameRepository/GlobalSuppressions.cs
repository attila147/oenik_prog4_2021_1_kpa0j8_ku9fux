﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<not a security issue.>", Scope = "member", Target = "~M:CircuitWars.GameRepository.IGameRepository.GetHighScores~System.Collections.Generic.List{CircuitWars.GameRepository.Result}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<not a security issue.>", Scope = "member", Target = "~M:CircuitWars.GameRepository.IGameRepository.GetLoadGames~System.Collections.Generic.List{System.String}")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRepository.GGameRepo.GetLoadNumber~System.Int32")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRepository.GGameRepo.Loads~System.Collections.Generic.List{System.String}")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRepository.GGameRepo.SaveHighscores(System.String,System.TimeSpan,System.String,System.Int32)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRepository.GGameRepo.GetHighScores~System.Collections.Generic.List{CircuitWars.GameRepository.Result}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameRepository.GGameRepo.Loads~System.Collections.Generic.List{System.String}")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "Uninteresting")]
