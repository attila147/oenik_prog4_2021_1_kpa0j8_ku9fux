﻿// <copyright file="Controller.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using CircuitWars.GameLogic;
    using CircuitWars.GameModel;

    /// <summary>
    /// Controller class.
    /// </summary>
    public class Controller : FrameworkElement
    {
        private FrameworkElement controller;
        private IGameModel model;
        private IGameLogic logic;
        private string level;
        private string playerName;
        private string mode;

        /// <summary>
        /// Initializes a new instance of the <see cref="Controller"/> class.
        /// </summary>
        public Controller()
        {
            this.Loaded += this.InitControl;
        }

        private void InitControl(object sender, RoutedEventArgs e)
        {
            MainWindow win = Window.GetWindow(this) as MainWindow;
            this.model = new GGameModel((double)this.ActualWidth, (double)this.ActualHeight);

            this.controller = new MenuControl(this.model);
            this.controller.Loaded += this.SwitchController;
            this.UpdateGrid();
        }

        private void UpdateGrid()
        {
            MainWindow win = Window.GetWindow(this) as MainWindow;
            if (win != null && this.controller != null)
            {
                win.Display.Children.Clear();
                if (win.FindName("header") != null)
                {
                    win.UnregisterName("header");
                }

                win.Display.Children.Add(this.controller);
            }
        }

        private void SwitchController(object sender, RoutedEventArgs e)
        {
            if (this.controller is GGameControl)
            {
                this.InitGameScreen();
            }
            else if (this.controller is MenuControl)
            {
                this.InitMenuScreen();
            }
            else if (this.controller is NewGameControl)
            {
                this.InitNewGameMenuScreen();
            }
            else if (this.controller is LoadGameControl)
            {
                this.InitLoadGameScreen();
            }
            else if (this.controller is HighScoreControl)
            {
                this.InitHighScoreScreen();
            }
        }

        private void InitHighScoreScreen()
        {
            StackPanel g = this.FindName("scores") as StackPanel;
            if (g != null)
            {
                foreach (var item in g.Children)
                {
                    foreach (var i in (item as StackPanel).Children)
                    {
                        foreach (var ij in (i as StackPanel).Children)
                        {
                            if (ij is Button && ((Button)ij).Name == "back")
                            {
                                ((Button)ij).Click += this.PBack_Click;
                            }
                        }
                    }
                }
            }
        }

        private void InitLoadGameScreen()
        {
            StackPanel g = this.FindName("loads") as StackPanel;
            if (g != null)
            {
                foreach (var item in g.Children)
                {
                    foreach (var i in (item as StackPanel).Children)
                    {
                        if (i is Button && ((Button)i).Name == "start")
                        {
                            ((Button)i).Click += this.StartLoad_Click;
                        }
                        else if (i is Button && ((Button)i).Name == "back")
                        {
                            ((Button)i).Click += this.PBack_Click;
                        }
                    }
                }
            }
        }

        private void StartLoad_Click(object sender, RoutedEventArgs e)
        {
            this.level = null;
            StackPanel s = this.FindName("loads") as StackPanel;
            if (s != null)
            {
                foreach (StackPanel stackPanel in s.Children)
                {
                    foreach (var item in stackPanel.Children)
                    {
                        if (item is ComboBox)
                        {
                            if ((item as ComboBox).Name == "level")
                            {
                                for (int i = 0; i < (item as ComboBox).Items.Count; i++)
                                {
                                    if ((item as ComboBox).SelectedItem != null)
                                    {
                                        this.level = (item as ComboBox).SelectedItem.ToString();
                                    }
                                    else
                                    {
                                        this.level = null;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (this.level != null)
            {
                MainWindow win = Window.GetWindow(sender as Button) as MainWindow;
                string fname = string.Empty;
                string[] path = Directory.GetCurrentDirectory().Split('\\');
                for (int i = 0; i < path.Length - 4; i++)
                {
                    fname += path[i] + "\\";
                }

                fname = fname + $"GameRepository\\Map\\{this.level}.xml";
                this.logic = new GGameLogic(this.model, fname);
                this.controller = new GGameControl(this.model, this.logic);
                this.controller.Loaded += this.SwitchController;
                this.UpdateGrid();
            }
        }

        private void InitNewGameMenuScreen()
        {
            StackPanel g = this.FindName("newGameMenu") as StackPanel;
            if (g != null)
            {
                foreach (var item in g.Children)
                {
                    foreach (var i in (item as StackPanel).Children)
                    {
                        if (i is Button && ((Button)i).Name == "start")
                        {
                            ((Button)i).Click += this.Start_Click;
                        }
                        else if (i is Button && ((Button)i).Name == "back")
                        {
                            ((Button)i).Click += this.PBack_Click;
                        }
                    }
                }
            }
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            StackPanel newGameMenu = this.FindName("newGameMenu") as StackPanel;
            if (newGameMenu != null)
            {
                foreach (StackPanel stackPanel in newGameMenu.Children)
                {
                    foreach (var item in stackPanel.Children)
                    {
                        if (item is TextBox)
                        {
                            if (!string.IsNullOrEmpty((item as TextBox).Text))
                            {
                                this.playerName = (item as TextBox).Text;
                            }
                            else
                            {
                                this.playerName = null;
                            }
                        }
                        else if (item is ComboBox)
                        {
                            if ((item as ComboBox).Name == "level")
                            {
                                for (int i = 0; i < (item as ComboBox).Items.Count; i++)
                                {
                                    if ((item as ComboBox).SelectedItem != null)
                                    {
                                        this.level = (item as ComboBox).SelectedItem.ToString();
                                    }
                                    else
                                    {
                                        this.level = null;
                                    }
                                }
                            }
                            else if ((item as ComboBox).Name == "mode")
                            {
                                for (int i = 0; i < (item as ComboBox).Items.Count; i++)
                                {
                                    if ((item as ComboBox).SelectedItem != null)
                                    {
                                        this.mode = (item as ComboBox).SelectedItem.ToString();
                                    }
                                    else
                                    {
                                        this.mode = null;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (this.level != null && this.playerName != null && this.mode != null)
            {
                if (this.mode == "easy")
                {
                    this.model.Mode = 3488;
                }
                else if (this.mode == "medium")
                {
                    this.model.Mode = 2608;
                }
                else if (this.mode == "hard")
                {
                    this.model.Mode = 1392;
                }

                MainWindow win = Window.GetWindow(sender as Button) as MainWindow;
                this.model.PlayerName = this.playerName;
                this.model.ActLevel = this.level;
                string fname = string.Empty;
                string[] path = Directory.GetCurrentDirectory().Split('\\');
                for (int i = 0; i < path.Length - 4; i++)
                {
                    fname += path[i] + "\\";
                }

                fname = fname + $"GameRepository\\Map\\{this.level}.xml";
                this.logic = new GGameLogic(this.model, fname);
                this.controller = new GGameControl(this.model, this.logic);
                this.controller.Loaded += this.SwitchController;
                this.UpdateGrid();
            }
        }

        private void InitMenuScreen()
        {
            StackPanel g = this.FindName("menu") as StackPanel;
            if (g != null)
            {
                foreach (var item in g.Children)
                {
                    Button b = (Button)item;
                    switch (b.Content)
                    {
                        case "New game":
                            b.Click += this.NewGame_Click;
                            break;
                        case "Load game":
                            b.Click += this.LoadGame_Click;
                            break;
                        case "Highscore":
                            b.Click += this.Highscore_Click;
                            break;
                        case "Exit":
                            b.Click += this.ExitGame_Click;
                            break;
                    }
                }
            }
        }

        private void ExitGame_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win = Window.GetWindow(this) as MainWindow;
            win.Close();
        }

        private void Highscore_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win = Window.GetWindow((Button)sender) as MainWindow;
            this.controller = new HighScoreControl(this.model);
            this.controller.Loaded += this.SwitchController;
            this.UpdateGrid();
        }

        private void LoadGame_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win = Window.GetWindow((Button)sender) as MainWindow;
            this.controller = new LoadGameControl(this.model);
            this.controller.Loaded += this.SwitchController;
            this.UpdateGrid();
        }

        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win = Window.GetWindow((Button)sender) as MainWindow;
            this.controller = new NewGameControl(this.model);
            this.controller.Loaded += this.SwitchController;
            this.UpdateGrid();
        }

        private void InitGameScreen()
        {
            Button g = this.FindName("b") as Button;
            if (g != null)
            {
                g.Click += this.B_Click;
            }

            StackPanel won = this.FindName("won") as StackPanel;
            if (won != null)
            {
                for (int i = 0; i < won.Children.Count; i++)
                {
                    if (won.Children[i] is Button)
                    {
                        if (((Button)won.Children[i]).Name == "restart")
                        {
                            ((Button)won.Children[i]).Click += this.PRestart_Click;
                        }
                        else if (((Button)won.Children[i]).Name == "back")
                        {
                            ((Button)won.Children[i]).Click += this.PBack_Click;
                        }
                    }
                }
            }

            StackPanel lose = this.FindName("lose") as StackPanel;
            if (lose != null)
            {
                for (int i = 0; i < lose.Children.Count; i++)
                {
                    if (lose.Children[i] is Button)
                    {
                        if (((Button)lose.Children[i]).Name == "restart")
                        {
                            ((Button)lose.Children[i]).Click += this.PRestart_Click;
                        }
                        else if (((Button)lose.Children[i]).Name == "back")
                        {
                            ((Button)lose.Children[i]).Click += this.PBack_Click;
                        }
                    }
                }
            }
        }

        private void B_Click(object sender, RoutedEventArgs e)
        {
            StackPanel pause = this.FindName("pause") as StackPanel;
            if (pause != null)
            {
                for (int i = 0; i < pause.Children.Count; i++)
                {
                    if (pause.Children[i] is Button)
                    {
                        if (((Button)pause.Children[i]).Name == "restart")
                        {
                            ((Button)pause.Children[i]).Click += this.PRestart_Click;
                        }
                        else if (((Button)pause.Children[i]).Name == "back")
                        {
                            ((Button)pause.Children[i]).Click += this.PBack_Click;
                        }
                    }
                }
            }
        }

        private void PBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win = Window.GetWindow(this) as MainWindow;
            this.controller = new MenuControl(this.model);
            this.controller.Loaded += this.SwitchController;
            this.UpdateGrid();
        }

        private void PRestart_Click(object sender, RoutedEventArgs e)
        {
            if (this.level != null && this.playerName != null && this.mode != null)
            {
                MainWindow win = Window.GetWindow(sender as Button) as MainWindow;
                this.model.PlayerName = this.playerName;
                this.model.ActLevel = this.level;
                if (this.mode == "easy")
                {
                    this.model.Mode = 3488;
                }
                else if (this.mode == "medium")
                {
                    this.model.Mode = 2608;
                }
                else if (this.mode == "hard")
                {
                    this.model.Mode = 1392;
                }

                string fname = string.Empty;
                string[] path = Directory.GetCurrentDirectory().Split('\\');
                for (int i = 0; i < path.Length - 4; i++)
                {
                    fname += path[i] + "\\";
                }

                fname = fname + $"GameRepository\\Map\\{this.level}.xml";
                this.logic = new GGameLogic(this.model, fname);
                this.controller = new GGameControl(this.model, this.logic);
                this.controller.Loaded += this.SwitchController;
                this.UpdateGrid();
            }
        }
    }
}
