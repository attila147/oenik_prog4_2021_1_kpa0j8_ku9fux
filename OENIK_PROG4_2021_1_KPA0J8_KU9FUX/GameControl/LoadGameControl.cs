﻿// <copyright file="LoadGameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameControl
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using CircuitWars.GameLogic;
    using CircuitWars.GameModel;
    using CircuitWars.GameRenderer;

    /// <summary>
    /// Load game control class.
    /// </summary>
    internal class LoadGameControl : FrameworkElement
    {
        private IGameModel model;
        private IGameLogic logic;
        private IGameRenderer gameRenderer;
        private MenuRenderer menuRenderer;
        private NewGameRenderer newGameRenderer;
        private LoadGameRenderer loadGameRenderer;
        private HighScoreRenderer scoreRenderer;
        private SmallWindowRenderer windowRenderer;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadGameControl"/> class.
        /// </summary>
        /// <param name="model">game model.</param>
        public LoadGameControl(IGameModel model)
        {
            this.model = model;
            this.Loaded += this.LoadGameControl_Loaded;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.gameRenderer != null && drawingContext != null)
            {
                drawingContext.DrawDrawing(this.gameRenderer.BuildDrawingMenu3());
            }
        }

        private void LoadGameControl_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindow win = Window.GetWindow(this) as MainWindow;
            if (win != null)
            {
                this.logic = new GGameLogic(this.model, string.Empty);
                this.menuRenderer = new MenuRenderer(this.model);
                this.newGameRenderer = new NewGameRenderer(this.model);
                this.loadGameRenderer = new LoadGameRenderer(this.model);
                this.scoreRenderer = new HighScoreRenderer(this.model);
                this.windowRenderer = new SmallWindowRenderer(this.model);
                this.gameRenderer = new GGameRenderer(this.model, this.menuRenderer, this.newGameRenderer, this.loadGameRenderer, this.scoreRenderer, this.windowRenderer);
                StackPanel loadGame = this.gameRenderer.GetLoadGames(this.logic.GetLoadGames());
                win.Display.Children.Add(loadGame);
                if (this.FindName("loads") != null)
                {
                    win.UnregisterName("loads");
                }

                win.RegisterName("loads", loadGame);
            }

            this.InvalidateVisual();
        }
    }
}