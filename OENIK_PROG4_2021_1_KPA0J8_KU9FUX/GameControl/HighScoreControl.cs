﻿// <copyright file="HighScoreControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using CircuitWars.GameLogic;
    using CircuitWars.GameModel;
    using CircuitWars.GameRenderer;

    /// <summary>
    /// Highscore control class.
    /// </summary>
    public class HighScoreControl : FrameworkElement
    {
        private IGameModel model;
        private IGameLogic logic;
        private IGameRenderer gameRenderer;
        private MenuRenderer menuRenderer;
        private NewGameRenderer newGameRenderer;
        private LoadGameRenderer loadGameRenderer;
        private HighScoreRenderer scoreRenderer;
        private SmallWindowRenderer windowRenderer;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreControl"/> class.
        /// </summary>
        /// <param name="model">game model.</param>
        public HighScoreControl(IGameModel model)
        {
            this.model = model;
            this.Loaded += this.HighScoreControl_Loaded;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.gameRenderer != null && drawingContext != null)
            {
                drawingContext.DrawDrawing(this.gameRenderer.BuildDrawingMenu4());
            }
        }

        private void HighScoreControl_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindow win = Window.GetWindow(this) as MainWindow;
            if (win != null)
            {
                this.logic = new GameLogic.GGameLogic(this.model, string.Empty);
                this.menuRenderer = new MenuRenderer(this.model);
                this.newGameRenderer = new NewGameRenderer(this.model);
                this.loadGameRenderer = new LoadGameRenderer(this.model);
                this.scoreRenderer = new HighScoreRenderer(this.model);
                this.windowRenderer = new SmallWindowRenderer(this.model);
                this.gameRenderer = new GameRenderer.GGameRenderer(this.model, this.menuRenderer, this.newGameRenderer, this.loadGameRenderer, this.scoreRenderer, this.windowRenderer);
                StackPanel highScore = this.gameRenderer.GetHighScores(this.logic.GetHighScores());
                win.Display.Children.Add(highScore);
                if (this.FindName("scores") != null)
                {
                    win.UnregisterName("scores");
                }

                win.RegisterName("scores", highScore);
            }

            this.InvalidateVisual();
        }
    }
}
