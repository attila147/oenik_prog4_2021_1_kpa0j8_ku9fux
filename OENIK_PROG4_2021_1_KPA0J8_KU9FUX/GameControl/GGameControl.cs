﻿// <copyright file="GGameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameControl
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Timers;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using CircuitWars.GameLogic;
    using CircuitWars.GameModel;
    using CircuitWars.GameRenderer;

    /// <summary>
    /// Game control class.
    /// </summary>
    public class GGameControl : FrameworkElement
    {
        private IGameLogic logic;
        private IGameRenderer gameRenderer;
        private IGameModel model;
        private Timer logicTimer;
        private MenuRenderer menuRenderer;
        private NewGameRenderer newGameRenderer;
        private LoadGameRenderer loadGameRenderer;
        private HighScoreRenderer scoreRenderer;
        private SmallWindowRenderer windowRenderer;
        private int now;
        private bool stillgoing = true;
        private GeometryGroup g;
        private LineGeometry lg;
        private DrawingGroup dg = new DrawingGroup();
        private Stopwatch dt = new Stopwatch();
        private Button b = new Button();
        private StackPanel stackPanel1;
        private StackPanel stackPanel2;
        private Label timer = new Label();

        /// <summary>
        /// Initializes a new instance of the <see cref="GGameControl"/> class.
        /// </summary>
        /// <param name="model">model.</param>
        /// <param name="logic">logic.</param>
        public GGameControl(IGameModel model, IGameLogic logic)
        {
            this.model = model;
            this.logic = logic;
            this.Loaded += this.GameControl_Loaded;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (drawingContext != null)
            {
                base.OnRender(drawingContext);
                this.Render();
                drawingContext.DrawDrawing(this.dg);
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindow win = Window.GetWindow(this) as MainWindow;
            this.menuRenderer = new MenuRenderer(this.model);
            this.loadGameRenderer = new LoadGameRenderer(this.model);
            this.newGameRenderer = new NewGameRenderer(this.model);
            this.scoreRenderer = new HighScoreRenderer(this.model);
            this.windowRenderer = new SmallWindowRenderer(this.model);
            this.gameRenderer = new GGameRenderer(this.model, this.menuRenderer, this.newGameRenderer, this.loadGameRenderer, this.scoreRenderer, this.windowRenderer);
            this.g = new GeometryGroup();
            this.now = 0;
            if (win != null)
            {
                win.MouseLeftButtonDown += this.Win_MouseLeftButtonDown;
                win.MouseLeftButtonUp += this.Win_MouseLeftButtonUp;
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("CircuitWars.GameControl.Image.pause.bmp");
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);
                this.b.Background = ib;
                this.b.Click += this.B_Click;
                this.b.HorizontalAlignment = HorizontalAlignment.Right;
                this.b.VerticalAlignment = VerticalAlignment.Top;
                this.b.Width = this.model.TileSize;
                this.b.Height = this.model.TileSize;
                this.b.BorderBrush = Brushes.Transparent;
                Label l1 = new Label();
                l1.Content = "Name:     " + this.model.PlayerName;
                l1.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
                l1.FontSize = 40;
                Label l2 = new Label();
                l2.Content = "Level:     " + this.model.ActLevel;
                l2.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
                l2.FontSize = 40;
                l2.HorizontalAlignment = HorizontalAlignment.Center;
                l2.VerticalAlignment = VerticalAlignment.Top;
                string modestring = string.Empty;
                if (this.model.Mode == 3488)
                {
                    modestring = "easy";
                }
                else if (this.model.Mode == 2608)
                {
                    modestring = "medium";
                }
                else if (this.model.Mode == 1392)
                {
                    modestring = "hard";
                }

                Label l3 = new Label();
                l3.Content = "Mode:     " + modestring;
                l3.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
                l3.FontSize = 40;
                l3.HorizontalAlignment = HorizontalAlignment.Right;
                l3.VerticalAlignment = VerticalAlignment.Bottom;
                win.Display.Children.Add(l2);
                win.Display.Children.Add(l1);
                win.Display.Children.Add(l3);
                win.Display.Children.Add(this.b);
                if (this.FindName("b") != null)
                {
                    win.UnregisterName("b");
                }

                win.RegisterName("b", this.b);
                if (this.FindName("pause") != null)
                {
                    win.UnregisterName("pause");
                }

                win.RegisterName("pause", new StackPanel());
                this.stackPanel1 = this.gameRenderer.GetWon(this.dt.Elapsed);
                if (this.FindName("won") != null)
                {
                    win.UnregisterName("won");
                }

                win.RegisterName("won", this.stackPanel1);

                this.stackPanel2 = this.gameRenderer.GetLose(this.dt.Elapsed);
                if (this.FindName("lose") != null)
                {
                    win.UnregisterName("lose");
                }

                win.RegisterName("lose", this.stackPanel2);
            }

            this.dt.Reset();
            this.Render();
            this.StartTimer();
            this.dt.Start();
        }

        private void B_Click(object sender, RoutedEventArgs e)
        {
            this.logicTimer.Stop();
            this.dt.Stop();
            MainWindow win = Window.GetWindow(this) as MainWindow;
            StackPanel stackPanel = this.gameRenderer.GetPause();
            win.Display.Children.Remove(stackPanel);
            win.Display.Children.Add(stackPanel);
            if (this.FindName("pause") != null)
            {
                win.UnregisterName("pause");
            }

            win.RegisterName("pause", stackPanel);
            StackPanel pause = this.FindName("pause") as StackPanel;
            if (pause != null)
            {
                for (int i = 0; i < pause.Children.Count; i++)
                {
                    if (pause.Children[i] is Button)
                    {
                        if (((Button)pause.Children[i]).Name == "resume")
                        {
                            ((Button)pause.Children[i]).Click += this.Resume_Click;
                        }
                        else if (((Button)pause.Children[i]).Name == "save")
                        {
                            ((Button)pause.Children[i]).Click += this.Save_Click;
                        }
                    }
                }
            }

            win.MouseLeftButtonDown -= this.Win_MouseLeftButtonDown;
            win.MouseLeftButtonUp -= this.Win_MouseLeftButtonUp;
        }

        private void Render()
        {
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    var drawingContext = this.dg.Open();
                    this.Render(drawingContext);
                    drawingContext.Close();
                });
            }
            catch (Exception)
            {
            }

            this.Timer();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.model.Time = this.dt.Elapsed;
            this.logic.Save();
            MessageBox.Show("Saved");
        }

        private void Resume_Click(object sender, RoutedEventArgs e)
        {
            this.logicTimer.Start();
            this.dt.Start();
            MainWindow win = Window.GetWindow(this) as MainWindow;
            StackPanel stackPanel = this.gameRenderer.GetPause();
            win.Display.Children.Remove(stackPanel);

            win.MouseLeftButtonDown += this.Win_MouseLeftButtonDown;
            win.MouseLeftButtonUp += this.Win_MouseLeftButtonUp;
        }

        private void StartTimer()
        {
            this.logicTimer = new Timer();
            this.logicTimer.Interval = 16;
            this.logicTimer.Elapsed += this.RunGame;
            this.logicTimer.Start();
        }

        private void RunGame(object sender, EventArgs e)
        {
            if (this.logic.StillGoing())
            {
                this.now += 16;
                this.Render();
                if (this.now % 16 == 0)
                {
                    this.logic.ChargeMovement();
                    this.logic.ChargeIntersect();
                }

                if (this.now % this.model.Mode == 0)
                {
                    this.logic.AiTick();
                }

                if (this.now % 1600 == 0)
                {
                    this.logic.ComponentTick();
                    this.logic.DeployElectrons();
                }

                if (this.now % 3488 == 0)
                {
                    this.now = 0;
                }

                this.stillgoing = this.logic.StillGoing();
            }
            else
            {
                this.logicTimer.Stop();
                this.dt.Stop();
                this.Render();
                if (this.logic.Whowon())
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        TimeSpan t = this.dt.Elapsed;
                        MainWindow win = Window.GetWindow(this) as MainWindow;
                        win.Display.Children.Add(this.stackPanel1);
                        this.logic.SaveHighScores(this.dt.Elapsed);
                        win.MouseLeftButtonDown -= this.Win_MouseLeftButtonDown;
                        win.MouseLeftButtonUp -= this.Win_MouseLeftButtonUp;
                        this.b.Click -= this.B_Click;
                    });
                }
                else
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        TimeSpan t = this.dt.Elapsed;
                        MainWindow win = Window.GetWindow(this) as MainWindow;
                        win.Display.Children.Add(this.stackPanel2);
                        win.MouseLeftButtonDown -= this.Win_MouseLeftButtonDown;
                        win.MouseLeftButtonUp -= this.Win_MouseLeftButtonUp;
                        this.b.Click -= this.B_Click;
                    });
                }
            }
        }

        private void Win_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var win = (Window)sender;
            if (win.CaptureMouse())
            {
                var startPoint = e.GetPosition(win);
                this.lg = new LineGeometry(startPoint, startPoint);
                this.g.Children.Add(this.lg);
                win.MouseMove += this.Win_MouseMove;
            }
        }

        private void Win_MouseMove(object sender, MouseEventArgs e)
        {
            var win = (Window)sender;

            if (win.IsMouseCaptured && e.LeftButton == MouseButtonState.Pressed)
            {
                LineGeometry lg = this.g.Children.OfType<LineGeometry>().LastOrDefault();

                if (lg != null)
                {
                    var endPoint = e.GetPosition(win);
                    lg.EndPoint = endPoint;
                }
            }
        }

        private void Win_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Window win = Window.GetWindow(this);
            ((Window)sender).ReleaseMouseCapture();
            this.logic.UserInputControl(this.lg.StartPoint, this.lg.EndPoint);
            this.g.Children.Remove(this.lg);
        }

        private void Render(DrawingContext drawingContext)
        {
            if (this.gameRenderer != null)
            {
                drawingContext.DrawDrawing(this.gameRenderer.BuildDrawing());
            }

            drawingContext.DrawGeometry(Brushes.Green, new Pen(Brushes.Green, 5), this.g);
        }

        private void Timer()
        {
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    MainWindow win = Window.GetWindow(this) as MainWindow;
                    var time = this.model.Time.Add(this.dt.Elapsed);
                    this.timer.Content = time.ToString(@"mm\:ss");
                    this.timer.Foreground = new SolidColorBrush(Color.FromRgb(0, 255, 0));
                    this.timer.FontSize = 40;
                    this.timer.HorizontalAlignment = HorizontalAlignment.Left;
                    this.timer.VerticalAlignment = VerticalAlignment.Bottom;
                    win.Display.Children.Remove(this.timer);
                    win.Display.Children.Add(this.timer);
                });
            }
            catch (Exception)
            {
            }
        }
    }
}
