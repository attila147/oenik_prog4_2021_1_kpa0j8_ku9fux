﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameControl.GGameControl.Timer")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameControl.GGameControl.Timer")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Uninteresting>", Scope = "member", Target = "~M:CircuitWars.GameControl.GGameControl.Render")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<Uninteresting>", Scope = "type", Target = "~T:CircuitWars.GameControl.GGameControl")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "Uninteresting")]
