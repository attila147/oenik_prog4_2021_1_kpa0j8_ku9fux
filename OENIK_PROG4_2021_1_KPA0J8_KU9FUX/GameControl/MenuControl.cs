﻿// <copyright file="MenuControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CircuitWars.GameControl
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Threading;
    using CircuitWars.GameLogic;
    using CircuitWars.GameModel;
    using CircuitWars.GameRenderer;

    /// <summary>
    /// Menu control class.
    /// </summary>
    internal class MenuControl : FrameworkElement
    {
        private IGameModel model;
        private IGameLogic logic;
        private IGameRenderer gameRenderer;
        private MenuRenderer menuRenderer;
        private NewGameRenderer newGameRenderer;
        private LoadGameRenderer loadGameRenderer;
        private HighScoreRenderer scoreRenderer;
        private SmallWindowRenderer windowRenderer;
        private DispatcherTimer dt;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuControl"/> class.
        /// </summary>
        /// <param name="model">game model.</param>
        public MenuControl(IGameModel model)
        {
            this.model = model;
            this.Loaded += this.Menu_Loaded;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.gameRenderer != null && drawingContext != null)
            {
                drawingContext.DrawDrawing(this.gameRenderer.BuildDrawingMenu());
            }
        }

        private void Menu_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindow win = Window.GetWindow(this) as MainWindow;

            if (win != null)
            {
                this.dt = new DispatcherTimer();
                this.logic = new GGameLogic(this.model, string.Empty);
                this.menuRenderer = new MenuRenderer(this.model);
                this.loadGameRenderer = new LoadGameRenderer(this.model);
                this.newGameRenderer = new NewGameRenderer(this.model);
                this.scoreRenderer = new HighScoreRenderer(this.model);
                this.windowRenderer = new SmallWindowRenderer(this.model);
                this.gameRenderer = new GGameRenderer(this.model, this.menuRenderer, this.newGameRenderer, this.loadGameRenderer, this.scoreRenderer, this.windowRenderer);
                StackPanel menu = this.gameRenderer.GetMenu();
                win.Display.Children.Add(menu);
                if (this.FindName("menu") != null)
                {
                    win.UnregisterName("menu");
                }

                win.RegisterName("menu", menu);
            }

            this.InvalidateVisual();
            this.dt.Interval = TimeSpan.FromMilliseconds(10);
            this.dt.Start();
        }
    }
}