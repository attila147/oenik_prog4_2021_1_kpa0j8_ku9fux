var interface_circuit_wars_1_1_game_model_1_1_i_game_model =
[
    [ "ActLevel", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html#a7539d9119896295353e61d7da987a69a", null ],
    [ "Components", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html#adefbc1730dc9906ad7475f77b3a77d7e", null ],
    [ "GameHeight", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html#a7b3b45df39852dc5645c4916dcea255d", null ],
    [ "GameWidth", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html#a09943046f6ad7c2f10ed92c966d17ea0", null ],
    [ "Mode", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html#acdd94d73cb63284482734aa47453107b", null ],
    [ "PlayerName", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html#a37afcc9ca12157d86316708707a0c545", null ],
    [ "Threads", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html#a61540e7fad706d8ac2ebc8ba7bc6b6f6", null ],
    [ "TileSize", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html#a46fa255e17ce9991be179234cf5863a1", null ],
    [ "Time", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html#a4014bfa977b26f6e18e0ce65cbbb1e8f", null ],
    [ "Walls", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html#a82ef6d37d053c8f39e4c62b691de213a", null ]
];