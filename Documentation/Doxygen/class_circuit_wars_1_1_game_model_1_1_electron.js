var class_circuit_wars_1_1_game_model_1_1_electron =
[
    [ "Electron", "class_circuit_wars_1_1_game_model_1_1_electron.html#a60e5697ee348f5e1f5185ef1a6901cf6", null ],
    [ "ActX", "class_circuit_wars_1_1_game_model_1_1_electron.html#ab9196bcda0b34227d45d759849768b58", null ],
    [ "ActY", "class_circuit_wars_1_1_game_model_1_1_electron.html#a90fb5305b5459535ead47d644452c832", null ],
    [ "Charge", "class_circuit_wars_1_1_game_model_1_1_electron.html#aaf6786ed0b12e794da3408430600af4f", null ],
    [ "Owner", "class_circuit_wars_1_1_game_model_1_1_electron.html#ae19ea695972568609a1b26e98638141f", null ],
    [ "Point", "class_circuit_wars_1_1_game_model_1_1_electron.html#ac0b29a04bf80e2bbbda2861ba2e9173e", null ],
    [ "Rect", "class_circuit_wars_1_1_game_model_1_1_electron.html#afdd3c98bdbd5df32895d42a18c401f15", null ],
    [ "StepX", "class_circuit_wars_1_1_game_model_1_1_electron.html#a6254c6fca4225004536cd11001bc59d9", null ],
    [ "StepY", "class_circuit_wars_1_1_game_model_1_1_electron.html#a49dbcee6b700dd89f496b3bf82239bac", null ]
];