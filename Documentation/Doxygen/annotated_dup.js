var annotated_dup =
[
    [ "CircuitWars", "namespace_circuit_wars.html", [
      [ "GameControl", "namespace_circuit_wars_1_1_game_control.html", [
        [ "App", "class_circuit_wars_1_1_game_control_1_1_app.html", null ],
        [ "Controller", "class_circuit_wars_1_1_game_control_1_1_controller.html", "class_circuit_wars_1_1_game_control_1_1_controller" ],
        [ "GGameControl", "class_circuit_wars_1_1_game_control_1_1_g_game_control.html", "class_circuit_wars_1_1_game_control_1_1_g_game_control" ],
        [ "HighScoreControl", "class_circuit_wars_1_1_game_control_1_1_high_score_control.html", "class_circuit_wars_1_1_game_control_1_1_high_score_control" ],
        [ "LoadGameControl", "class_circuit_wars_1_1_game_control_1_1_load_game_control.html", "class_circuit_wars_1_1_game_control_1_1_load_game_control" ],
        [ "MainWindow", "class_circuit_wars_1_1_game_control_1_1_main_window.html", "class_circuit_wars_1_1_game_control_1_1_main_window" ],
        [ "MenuControl", "class_circuit_wars_1_1_game_control_1_1_menu_control.html", "class_circuit_wars_1_1_game_control_1_1_menu_control" ],
        [ "NewGameControl", "class_circuit_wars_1_1_game_control_1_1_new_game_control.html", "class_circuit_wars_1_1_game_control_1_1_new_game_control" ]
      ] ],
      [ "GameLogic", "namespace_circuit_wars_1_1_game_logic.html", [
        [ "GGameLogic", "class_circuit_wars_1_1_game_logic_1_1_g_game_logic.html", "class_circuit_wars_1_1_game_logic_1_1_g_game_logic" ],
        [ "IGameLogic", "interface_circuit_wars_1_1_game_logic_1_1_i_game_logic.html", "interface_circuit_wars_1_1_game_logic_1_1_i_game_logic" ]
      ] ],
      [ "GameLogicTests", "namespace_circuit_wars_1_1_game_logic_tests.html", [
        [ "LogicTests", "class_circuit_wars_1_1_game_logic_tests_1_1_logic_tests.html", "class_circuit_wars_1_1_game_logic_tests_1_1_logic_tests" ],
        [ "ModelSetup", "class_circuit_wars_1_1_game_logic_tests_1_1_model_setup.html", "class_circuit_wars_1_1_game_logic_tests_1_1_model_setup" ]
      ] ],
      [ "GameModel", "namespace_circuit_wars_1_1_game_model.html", [
        [ "Component", "class_circuit_wars_1_1_game_model_1_1_component.html", "class_circuit_wars_1_1_game_model_1_1_component" ],
        [ "ComponentShadow", "class_circuit_wars_1_1_game_model_1_1_component_shadow.html", "class_circuit_wars_1_1_game_model_1_1_component_shadow" ],
        [ "Electron", "class_circuit_wars_1_1_game_model_1_1_electron.html", "class_circuit_wars_1_1_game_model_1_1_electron" ],
        [ "GGameModel", "class_circuit_wars_1_1_game_model_1_1_g_game_model.html", "class_circuit_wars_1_1_game_model_1_1_g_game_model" ],
        [ "IGameModel", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html", "interface_circuit_wars_1_1_game_model_1_1_i_game_model" ],
        [ "Thread", "class_circuit_wars_1_1_game_model_1_1_thread.html", "class_circuit_wars_1_1_game_model_1_1_thread" ],
        [ "Wall", "class_circuit_wars_1_1_game_model_1_1_wall.html", "class_circuit_wars_1_1_game_model_1_1_wall" ]
      ] ],
      [ "GameRenderer", "namespace_circuit_wars_1_1_game_renderer.html", [
        [ "GGameRenderer", "class_circuit_wars_1_1_game_renderer_1_1_g_game_renderer.html", "class_circuit_wars_1_1_game_renderer_1_1_g_game_renderer" ],
        [ "HighScoreRenderer", "class_circuit_wars_1_1_game_renderer_1_1_high_score_renderer.html", "class_circuit_wars_1_1_game_renderer_1_1_high_score_renderer" ],
        [ "IGameRenderer", "interface_circuit_wars_1_1_game_renderer_1_1_i_game_renderer.html", "interface_circuit_wars_1_1_game_renderer_1_1_i_game_renderer" ],
        [ "LoadGameRenderer", "class_circuit_wars_1_1_game_renderer_1_1_load_game_renderer.html", "class_circuit_wars_1_1_game_renderer_1_1_load_game_renderer" ],
        [ "MenuRenderer", "class_circuit_wars_1_1_game_renderer_1_1_menu_renderer.html", "class_circuit_wars_1_1_game_renderer_1_1_menu_renderer" ],
        [ "NewGameRenderer", "class_circuit_wars_1_1_game_renderer_1_1_new_game_renderer.html", "class_circuit_wars_1_1_game_renderer_1_1_new_game_renderer" ],
        [ "SmallWindowRenderer", "class_circuit_wars_1_1_game_renderer_1_1_small_window_renderer.html", "class_circuit_wars_1_1_game_renderer_1_1_small_window_renderer" ]
      ] ],
      [ "GameRepository", "namespace_circuit_wars_1_1_game_repository.html", [
        [ "GGameRepo", "class_circuit_wars_1_1_game_repository_1_1_g_game_repo.html", "class_circuit_wars_1_1_game_repository_1_1_g_game_repo" ],
        [ "IGameRepository", "interface_circuit_wars_1_1_game_repository_1_1_i_game_repository.html", "interface_circuit_wars_1_1_game_repository_1_1_i_game_repository" ],
        [ "Result", "class_circuit_wars_1_1_game_repository_1_1_result.html", "class_circuit_wars_1_1_game_repository_1_1_result" ]
      ] ]
    ] ],
    [ "GameControl", "namespace_game_control.html", [
      [ "App", "class_game_control_1_1_app.html", "class_game_control_1_1_app" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];