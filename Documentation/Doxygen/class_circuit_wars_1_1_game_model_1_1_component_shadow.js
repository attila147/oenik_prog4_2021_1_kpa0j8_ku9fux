var class_circuit_wars_1_1_game_model_1_1_component_shadow =
[
    [ "ComponentShadow", "class_circuit_wars_1_1_game_model_1_1_component_shadow.html#ae2fabfeb30544edbb80f4746dbb0374c", null ],
    [ "ChargeLevel", "class_circuit_wars_1_1_game_model_1_1_component_shadow.html#a0f2371ff12fa30bcbfbbb9e0406f4a8d", null ],
    [ "ConnectedIds", "class_circuit_wars_1_1_game_model_1_1_component_shadow.html#ab646178f335a7a8548142627589ccc72", null ],
    [ "ConnectedTOhim", "class_circuit_wars_1_1_game_model_1_1_component_shadow.html#af5b33d477b576eba3ccff0f27836077a", null ],
    [ "ConnectedTOus", "class_circuit_wars_1_1_game_model_1_1_component_shadow.html#abcb5347893b2985152789ce7001f7045", null ],
    [ "Id", "class_circuit_wars_1_1_game_model_1_1_component_shadow.html#a4f85bde184de7ae070a78f5015af21fc", null ],
    [ "IsAttacked", "class_circuit_wars_1_1_game_model_1_1_component_shadow.html#abe759399305ab08bce7553f734a762b1", null ],
    [ "Owner", "class_circuit_wars_1_1_game_model_1_1_component_shadow.html#abdae40dd7ead169ae6c03311d95db292", null ]
];