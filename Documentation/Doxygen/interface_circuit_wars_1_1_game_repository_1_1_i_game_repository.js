var interface_circuit_wars_1_1_game_repository_1_1_i_game_repository =
[
    [ "GetHighScores", "interface_circuit_wars_1_1_game_repository_1_1_i_game_repository.html#a22392fa9c3a61221f9431cf09791c15f", null ],
    [ "GetLoadGames", "interface_circuit_wars_1_1_game_repository_1_1_i_game_repository.html#af997b03e792676dad14da25b474f2c62", null ],
    [ "GetLoadNumber", "interface_circuit_wars_1_1_game_repository_1_1_i_game_repository.html#a361a2d83ea01e7048ea4616798837274", null ],
    [ "SaveGame", "interface_circuit_wars_1_1_game_repository_1_1_i_game_repository.html#acc653a791afd3affc544e88a5c25d28e", null ],
    [ "SaveHighscores", "interface_circuit_wars_1_1_game_repository_1_1_i_game_repository.html#a711d31060cd7870dc1054060dc55d0eb", null ]
];