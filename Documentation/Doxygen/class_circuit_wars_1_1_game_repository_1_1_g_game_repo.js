var class_circuit_wars_1_1_game_repository_1_1_g_game_repo =
[
    [ "AddLoadXml", "class_circuit_wars_1_1_game_repository_1_1_g_game_repo.html#aa0b4d0c11f4558236a45beaab90e1375", null ],
    [ "GetHighScores", "class_circuit_wars_1_1_game_repository_1_1_g_game_repo.html#ae03cb5f34fa5487e36fe427e0dcc1e0c", null ],
    [ "GetLoadGames", "class_circuit_wars_1_1_game_repository_1_1_g_game_repo.html#a0b3cc3c2fb8673fd638ce19b3cf182c0", null ],
    [ "GetLoadNumber", "class_circuit_wars_1_1_game_repository_1_1_g_game_repo.html#a65fdafb98002289a82c1ef427bf30ba8", null ],
    [ "Loads", "class_circuit_wars_1_1_game_repository_1_1_g_game_repo.html#a3ae4cbcfdfbb14fffaab334643054086", null ],
    [ "SaveGame", "class_circuit_wars_1_1_game_repository_1_1_g_game_repo.html#a6f8210dec80e308b042999c83f59ff4a", null ],
    [ "SaveHighscores", "class_circuit_wars_1_1_game_repository_1_1_g_game_repo.html#a06f20fd9edb689fa21ce54c914cfd9e0", null ]
];