var hierarchy =
[
    [ "Application", null, [
      [ "CircuitWars.GameControl.App", "class_circuit_wars_1_1_game_control_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "GameControl.App", "class_game_control_1_1_app.html", null ],
      [ "GameControl.App", "class_game_control_1_1_app.html", null ]
    ] ],
    [ "CircuitWars.GameModel.Component", "class_circuit_wars_1_1_game_model_1_1_component.html", null ],
    [ "CircuitWars.GameModel.ComponentShadow", "class_circuit_wars_1_1_game_model_1_1_component_shadow.html", null ],
    [ "CircuitWars.GameModel.Electron", "class_circuit_wars_1_1_game_model_1_1_electron.html", null ],
    [ "FrameworkElement", null, [
      [ "CircuitWars.GameControl.Controller", "class_circuit_wars_1_1_game_control_1_1_controller.html", null ],
      [ "CircuitWars.GameControl.GGameControl", "class_circuit_wars_1_1_game_control_1_1_g_game_control.html", null ],
      [ "CircuitWars.GameControl.HighScoreControl", "class_circuit_wars_1_1_game_control_1_1_high_score_control.html", null ],
      [ "CircuitWars.GameControl.LoadGameControl", "class_circuit_wars_1_1_game_control_1_1_load_game_control.html", null ],
      [ "CircuitWars.GameControl.MenuControl", "class_circuit_wars_1_1_game_control_1_1_menu_control.html", null ],
      [ "CircuitWars.GameControl.NewGameControl", "class_circuit_wars_1_1_game_control_1_1_new_game_control.html", null ]
    ] ],
    [ "CircuitWars.GameRenderer.HighScoreRenderer", "class_circuit_wars_1_1_game_renderer_1_1_high_score_renderer.html", null ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "CircuitWars.GameControl.MainWindow", "class_circuit_wars_1_1_game_control_1_1_main_window.html", null ],
      [ "CircuitWars.GameControl.MainWindow", "class_circuit_wars_1_1_game_control_1_1_main_window.html", null ]
    ] ],
    [ "CircuitWars.GameLogic.IGameLogic", "interface_circuit_wars_1_1_game_logic_1_1_i_game_logic.html", [
      [ "CircuitWars.GameLogic.GGameLogic", "class_circuit_wars_1_1_game_logic_1_1_g_game_logic.html", null ]
    ] ],
    [ "CircuitWars.GameModel.IGameModel", "interface_circuit_wars_1_1_game_model_1_1_i_game_model.html", [
      [ "CircuitWars.GameModel.GGameModel", "class_circuit_wars_1_1_game_model_1_1_g_game_model.html", null ]
    ] ],
    [ "CircuitWars.GameRenderer.IGameRenderer", "interface_circuit_wars_1_1_game_renderer_1_1_i_game_renderer.html", [
      [ "CircuitWars.GameRenderer.GGameRenderer", "class_circuit_wars_1_1_game_renderer_1_1_g_game_renderer.html", null ]
    ] ],
    [ "CircuitWars.GameRepository.IGameRepository", "interface_circuit_wars_1_1_game_repository_1_1_i_game_repository.html", [
      [ "CircuitWars.GameRepository.GGameRepo", "class_circuit_wars_1_1_game_repository_1_1_g_game_repo.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "CircuitWars.GameRenderer.LoadGameRenderer", "class_circuit_wars_1_1_game_renderer_1_1_load_game_renderer.html", null ],
    [ "CircuitWars.GameLogicTests.LogicTests", "class_circuit_wars_1_1_game_logic_tests_1_1_logic_tests.html", null ],
    [ "CircuitWars.GameRenderer.MenuRenderer", "class_circuit_wars_1_1_game_renderer_1_1_menu_renderer.html", null ],
    [ "CircuitWars.GameLogicTests.ModelSetup", "class_circuit_wars_1_1_game_logic_tests_1_1_model_setup.html", null ],
    [ "CircuitWars.GameRenderer.NewGameRenderer", "class_circuit_wars_1_1_game_renderer_1_1_new_game_renderer.html", null ],
    [ "CircuitWars.GameRepository.Result", "class_circuit_wars_1_1_game_repository_1_1_result.html", null ],
    [ "CircuitWars.GameRenderer.SmallWindowRenderer", "class_circuit_wars_1_1_game_renderer_1_1_small_window_renderer.html", null ],
    [ "CircuitWars.GameModel.Thread", "class_circuit_wars_1_1_game_model_1_1_thread.html", null ],
    [ "CircuitWars.GameModel.Wall", "class_circuit_wars_1_1_game_model_1_1_wall.html", null ],
    [ "System.Windows.Window", null, [
      [ "CircuitWars.GameControl.MainWindow", "class_circuit_wars_1_1_game_control_1_1_main_window.html", null ],
      [ "CircuitWars.GameControl.MainWindow", "class_circuit_wars_1_1_game_control_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "CircuitWars.GameControl.MainWindow", "class_circuit_wars_1_1_game_control_1_1_main_window.html", null ]
    ] ]
];